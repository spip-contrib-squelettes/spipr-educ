<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_spipr_educ_configure_menu_rubriques_3_colonnes_charger_dist($nom,$id) {
	$tab_sql=sql_fetsel('*','spip_spipr_educ',"nom='rubrique_menu_rubriques_3_colonnes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$valeurs['menu3colonnesBackground'] = $tab_sql['parametre5'];
	$valeurs['menu3colonnesDegrade'] = $tab_sql['parametre6'];
	$valeurs['menu3colonnesLinkColor'] = $tab_sql['parametre7'];
	$valeurs['menu3colonnesCSS'] = $tab_sql['parametre8'];
	$valeurs['nom'] = $nom;
	return $valeurs;
}

function formulaires_spipr_educ_configure_menu_rubriques_3_colonnes_traiter_dist($nom,$id) {
	if (_request('hidden_menu3colonnes') == 'ok') {
		sql_updateq(
			'spip_spipr_educ',
			array(
				'parametre5' => _request('menu3colonnesBackground'),
				'parametre6' => _request('menu3colonnesDegrade'),
				'parametre7' => _request('menu3colonnesLinkColor'),
				'parametre8' => _request('menu3colonnesCSS'),
			),
			"nom='rubrique_menu_rubriques_3_colonnes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
		);
	}
	$res['message_ok'] = _T('config_info_enregistree');
	return $res;
}