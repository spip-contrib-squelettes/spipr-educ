<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_spipr_educ_themes_charger_dist() {
	// Layout pr�c�demment choisi
	$nom_theme = sql_getfetsel('nom','spip_spipr_educ',"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$chemin = _DIR_PLUGIN_SPIPR_EDUC.'themes/';
	$valeurs = array();
	$valeurs['theme_actuel'] = $nom_theme;
	$valeurs['chemin'] = $chemin;
	return $valeurs;
}

function formulaires_spipr_educ_themes_traiter_dist() {
	include_spip('inc/spipr_educ_definitions_themes');
	$themes_proposes = spipr_educ_definition_themes();
	$res = array('editable'=>true);
	// On teste le th�me actuel : on ne fait rien s'il n'y a pas de changement de th�me
	$nom_theme_actuel = sql_getfetsel('nom','spip_spipr_educ',"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	if ($nom_theme_actuel == $themes_proposes[_request('theme_choix',$_POST)]) { $res['message_ok'] = "Vous conservez le th&egrave;me actuel, aucune modification n'a &eacute;t&eacute; enregistr&eacute;e.";}
	else {
		foreach($themes_proposes as  $cle => $nom_theme) {
			if (_request('theme_choix',$_POST) == $cle) {
				sql_updateq(
					'spip_spipr_educ',
					array(
						'nom' => $nom_theme,
					),
					"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				spipr_educ_reinitialiser_theme($themes_proposes[_request('theme_choix',$_POST)]);
			}
		}
		$res['message_ok'] = _T('config_info_enregistree');
	}
	return $res;
}