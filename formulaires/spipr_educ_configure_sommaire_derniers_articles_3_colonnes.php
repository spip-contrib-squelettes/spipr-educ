<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_spipr_educ_configure_sommaire_derniers_articles_3_colonnes_charger_dist($nom,$id) {
	$tab_sql=sql_fetsel('*','spip_spipr_educ',"nom='sommaire_derniers_articles_3_colonnes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND type='graphisme'");
	$valeurs['derniersarticles3colonnesBlockTitle'] = $tab_sql['parametre1'];
	$valeurs['derniersarticles3colonnesBackground'] = $tab_sql['parametre2'];
	$valeurs['derniersarticles3colonnesPaging'] = $tab_sql['parametre3'];
	$valeurs['derniersarticles3colonnesTextButton'] = $tab_sql['parametre4'];
	$valeurs['derniersarticles3colonnesButtonColor'] = $tab_sql['parametre5'];
	$valeurs['derniersarticles3colonnesButtonBackground'] = $tab_sql['parametre6'];
	$valeurs['derniersarticles3colonnesButtonLighten'] = $tab_sql['parametre7'];
	$valeurs['derniersarticles3colonnesButtonDarken'] = $tab_sql['parametre8'];
	$valeurs['derniersarticles3colonnesCss'] = $tab_sql['parametre9'];
	$valeurs['nom'] = $nom;
	return $valeurs;
}

function formulaires_spipr_educ_configure_sommaire_derniers_articles_3_colonnes_traiter_dist($nom,$id) {
	if (_request('hidden_derniersarticles3colonnes') == 'ok') {
		sql_updateq(
			'spip_spipr_educ',
			array(
				'parametre1' => _request('derniersarticles3colonnesBlockTitle'),
				'parametre2' => _request('derniersarticles3colonnesBackground'),
				'parametre3' => _request('derniersarticles3colonnesPaging'),
				'parametre4' => _request('derniersarticles3colonnesTextButton'),
				'parametre5' => _request('derniersarticles3colonnesButtonColor'),
				'parametre6' => _request('derniersarticles3colonnesButtonBackground'),
				'parametre7' => _request('derniersarticles3colonnesButtonLighten'),
				'parametre8' => _request('derniersarticles3colonnesButtonDarken'),
				'parametre9' => _request('derniersarticles3colonnesCss'),
			),
			"nom='sommaire_derniers_articles_3_colonnes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND type='graphisme'"
		);
	}
	$res['message_ok'] = _T('config_info_enregistree');
	return $res;
}