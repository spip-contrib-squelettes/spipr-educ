<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_spipr_educ_configure_sommaire_carousel_slick_charger_dist() {
	include_spip('inc/spipr_educ_definitions_themes');
	$test_couleur=sql_select('parametre1','spip_spipr_educ',"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$tab_couleur=sql_fetch($test_couleur);
	$couleur=$tab_couleur['parametre1'];
	$retour_couleur=spipr_educ_definition_couleurs_theme_educnat($couleur);
	$code_couleur=$retour_couleur[0];
	
	$tab_sql = sql_fetsel('*','spip_spipr_educ',"nom='sommaire_carousel_slick' AND type='bloc de base' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND parametre1='sommaire'");
	$valeurs['carouselSlickH2title'] = ($tab_sql['parametre5'] ? $tab_sql['parametre5'] : 'À la Une');
	$valeurs['carouselSlickH2color'] = ($tab_sql['parametre6'] ? $tab_sql['parametre6'] : $code_couleur);
	$valeurs['carouselSlickH3color'] = ($tab_sql['parametre7'] ? $tab_sql['parametre7'] : '#333');
	$valeurs['carouselSlickCss'] = ($tab_sql['parametre8'] ? $tab_sql['parametre8'] : '');
	
	$OrdreArticles = sql_getfetsel('parametre3','spip_spipr_educ',"nom='options_articles' AND type='gestion bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$valeurs['carouselOrdreArticles']= ($OrdreArticles ? $OrdreArticles : '');
	
	$tab_sql2 = sql_fetsel('parametre5','spip_spipr_educ',"nom='graphisme_carousel_2' AND type='graphisme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$valeurs['carouselNombreActus'] = ($tab_sql2['parametre5'] ? $tab_sql2['parametre5'] : '5');

	return $valeurs;
}

function formulaires_spipr_educ_configure_sommaire_carousel_slick_traiter_dist() {
	if (_request('hidden_carousel') == 'ok') {
		sql_updateq(
			'spip_spipr_educ',
			array(
				'parametre5' => _request('carouselSlickH2title'),
				'parametre6' => _request('carouselSlickH2color'),
				'parametre7' => _request('carouselSlickH3color'),
				'parametre8' => _request('carouselSlickCss'),
			),
			"nom='sommaire_carousel_slick' AND type='bloc de base' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND parametre1='sommaire'"
		);
		
		sql_updateq(
			'spip_spipr_educ',
			array(
				'parametre5' => _request('carouselNombreActus'),
			),
			"nom='graphisme_carousel_2' AND type='graphisme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
		);
		
		include_spip('inc/spipr_educ_traitements_listes');
		$test_articles_une = sql_select('*','spip_spipr_educ',"nom='options_articles' AND type='gestion bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
		$tab_articles_une = sql_fetch($test_articles_une);
		$chaine = $tab_articles_une['parametre3'];
		$chaine_retour = $chaine;
		$tab = explode(",",$chaine);
		foreach ($tab as $id) {
			if (is_numeric(_request('supprimer_site_'.$id.'_x'))) {
				$chaine_retour=spipr_educ_liste_del($chaine,$id);
			}
			if (is_numeric(_request('monter_site_'.$id.'_x'))) {
				$chaine_retour=spipr_educ_liste_up($chaine,$id);
			}
			if (is_numeric(_request('descendre_site_'.$id.'_x'))) {
				$chaine_retour=spipr_educ_liste_down($chaine,$id);
			}
		}
		sql_updateq('spip_spipr_educ',array('parametre3' => $chaine_retour),"nom='options_articles' AND type='gestion bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
		
	}
	$res['message_ok'] = _T('config_info_enregistree');
	return $res;
}