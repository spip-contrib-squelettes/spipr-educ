<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_spipr_educ_css_charger_dist() {
	$cssless = sql_getfetsel('parametre1','spip_spipr_educ',"nom='graphisme_css' AND type='graphisme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$valeurs['CSS-LESS'] = ($cssless ? $cssless : '');
	return $valeurs;
}

function formulaires_spipr_educ_css_traiter_dist() {
	if (_request('hidden_css') == 'ok') {
		sql_updateq(
			'spip_spipr_educ',
			array(
				'parametre1' => _request('CSS-LESS'),
			),
			"nom='graphisme_css' AND type='graphisme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
		);
	}
	$res['message_ok'] = _T('config_info_enregistree');
	return $res;
}