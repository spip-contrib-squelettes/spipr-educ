<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_spipr_educ_recherche_educnat_charger_dist() {
	$mots_cles = sql_getfetsel('parametre1','spip_spipr_educ',"nom='recherche_educnat' AND type='gestion bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$valeurs['rechercheEducnat'] = ($mots_cles ? $mots_cles : '');
	return $valeurs;
}

function formulaires_spipr_educ_recherche_educnat_traiter_dist() {
	$decompte = 0;
	$tab_request = array();
	if (_request('hidden_recherche_educnat') == 'ok') {
		$req_mots_cles = sql_select('id_mot','spip_mots','','titre');
		while ($mot = sql_fetch($req_mots_cles)){
			if (_request('mot'.$mot['id_mot']) == 'on') {
				$tab_request[] = $mot['id_mot'];
				$decompte++;
			}
		};
		if ($decompte > 10) {$res['message_erreur'] = "Merci de choisir moins de 10 mots-clés";}
		else {
			$res['message_ok'] = _T('config_info_enregistree');
			sql_updateq('spip_spipr_educ',array('parametre1' => implode(",",$tab_request)),"nom='recherche_educnat' AND type='gestion bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
		}
	}
	return $res;
}