<?php
// On est dans le thème 'educnat', on s'assure que la noisette de menu des rubriques en 3 colonnes est bien présente. Si non, on la recrée ainsi que les paramètres liés au thème de couleur.
$test_3_colonnes=sql_select('nom','spip_spipr_educ',"type='bloc de base' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND parametre1='rubrique' AND nom='rubrique_menu_rubriques_3_colonnes'");
$tab_3_colonnes=sql_fetch($test_3_colonnes);
if ($tab_3_colonnes['nom']!='rubrique_menu_rubriques_3_colonnes') {
	
	// Recherche de la couleur actuelle
	include_spip('inc/spipr_educ_definitions_themes');
	$test_couleur=sql_select('parametre1','spip_spipr_educ',"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$tab_couleur=sql_fetch($test_couleur);
	$couleur=$tab_couleur['parametre1'];
	$retour_couleur=spipr_educ_definition_couleurs_theme_educnat($couleur);
	$code_couleur=$retour_couleur[0];
	$pourcentage_degrade_couleur='10';
	$code_couleur_lien='#1d88cb';
	$css='/* -- Spécifique menu rubrique -- */
#bloc_rubriques_3_colonnes {
display:block;
position:relative;
margin:50px 0;
padding:0;
width:100%;
height:auto;
#bloc_rubriques_3_colonnes_titre_principal {
display:block;
position:relative;
padding:29px 80px 10px 80px;
@media (max-width: 600px){
padding:29px 40px 10px 40px;
}
.container_h2 {
display:block;
width:100%;
height:auto;
h2.h2 {
color:#fff;
font-size:22px;
line-height:28px;
vertical-align:top;
margin-bottom:5px;
}
h2.h2::before {background-color:#fff;}
}
}

#container_bloc_rubrique_colonne {
display:flex;
flex-wrap: wrap;
justify-content: flex-start;
padding:0 70px 70px 70px;
@media (max-width: 600px){padding:0 30px 30px 30px;}

.bloc_rubrique_colonne{
width:~"calc((100% - 180px) / 3)";
@media (max-width: 979px){width:~"calc((100% - 120px) / 2)";}
@media (max-width: 600px){width:~"calc(100% - 60px)";}
margin:10px;
padding:20px 20px 30px 20px;
height:auto;
background-color:#fff;

.bloc_rubrique_colonne_titre{
display:block;
position:relative;
width:100%;
font-size:15px;
line-height:22px;
vertical-align:top;
margin-bottom:10px;
a, .bloc_rubrique_colonne_descriptif {color:#222;}
a{text-transform: uppercase; font-weight:600;}
}

.bloc_rubrique_colonne_descriptif {
color:#333;
line-height:20px;
text-align:top;
margin-bottom:18px;
}
ul.bloc_rubrique_colonne_sous_rubriques {
list-style-position: inside;
margin:0 0 20px 20px;
}
}
}
}';
	
	// Création de l'entrée noisette
	sql_insertq('spip_spipr_educ',array(
		'nom'=>'rubrique_menu_rubriques_3_colonnes',
		'type'=>'bloc de base',
		'nom_sauvegarde'=>'en_cours_d_utilisation_SPIPr',
		'parametre1'=>'rubrique',
		'parametre2'=>'off',
		'parametre3'=>'1',
		'parametre4'=>'cfg',
		'parametre5'=>$code_couleur,
		'parametre6'=>$pourcentage_degrade_couleur,
		'parametre7'=>$code_couleur_lien,	
		'parametre8'=>$css,	
	));
}
// Même travail avec la présentation des derniers articles en 3 colonnes en page de sommaire
$test_articles_3_colonnes=sql_select('nom','spip_spipr_educ',"type='bloc de base' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND parametre1='sommaire' AND nom='sommaire_derniers_articles_3_colonnes'");
$tab_articles_3_colonnes=sql_fetch($test_articles_3_colonnes);
if ($tab_articles_3_colonnes['nom']!='sommaire_derniers_articles_3_colonnes') {
	// Recherche de la couleur actuelle et l'ensemble des paramètres
	include_spip('inc/spipr_educ_definitions_themes');
	$test_couleur=sql_select('parametre1','spip_spipr_educ',"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$tab_couleur=sql_fetch($test_couleur);
	$couleur=$tab_couleur['parametre1'];
	$retour_couleur=spipr_educ_definition_couleurs_theme_educnat($couleur);
	$code_couleur=$retour_couleur[0];
	$pourcentage_degrade_couleur='10';
	$css="/*-- Module 3 colonnes, articles en page de sommaire --*/ 
#sommaire_derniers_articles_3_colonnes{
display:block;
position:relative;
width:100%;
height:auto;
margin:50px 0;
padding:0;

.container_articles_3_colonnes_titre {
display:block;
position:relative;
width:100%;
height:auto;
margin:0;
padding:0;
}

ul.container_articles_3_colonnes {
display:flex;
flex-wrap: wrap;
justify-content: space-between;
margin:0;
padding:0;
list-style:none;
& > li {
display:flex;
flex-direction: column;
flex-wrap: nowrap;
justify-content: space-between;
align-items:first baseline last baseline;
width:~\"calc((100% - 46px) / 3)\";
@media (min-width:768px) and (max-width:979px) {
width:~\"calc(50% - 12px)\";
}
@media (max-width:767px) {
width:~\"calc(100% - 2px)\";
}
height:auto;
border:1px solid #eaeaea;
margin:0 0 20px 0;
padding:0;
.trois_colonnes_container_haut {
.article_3_colonnes_logo {
width:100%;
height:auto;
margin:0;
padding:0;
a {
margin:0;
padding:0;
img {
width:100%;
height:auto;
margin:0;
padding:0;
}
}
}
.article_3_colonnes_mots_cles_container{
display:block;
position:relative;
margin:18px 17px 26px 17px;
ul.article_3_colonnes_mots_cles{
display:inline-flex;
flex-direction: row;
flex-wrap: wrap;
justify-content: start;
list-style:none;
margin:0;
padding:0;
li {
display: list-item inline;
margin:4px;
padding:0;
border:none;
a {
display: inline-block;
font-size: 16px;
line-height: 16px;
padding: 6px 14px;
color: #333;
background-color:#fff;
border: 2px solid #eaeaea;
border-radius: 28px;
white-space: nowrap;
transition: background-color 400ms, color 400ms;}
a:hover{
text-decoration:none;
color:#fff;}
}
}
}
h3.article_3_colonnes_titre{
margin:20px 25px 8px 25px;
line-height:30px;
font-size:18px;
vertical-align:top;
font-family:'Archive', Arial, Helvetica, sans-serif;
	a{color:#333;}
}
.article_3_colonnes_maj{
margin:0 25px;
font-style: italic;
color:#9b9b9b;
font-size:12px;
}
.article_3_colonnes_introduction{
margin:25px;
line-height:26px;
font-size:14px;
vertical-align:top;
}
}
.trois_colonnes_container_bas{
text-align:center;
margin:25px;
a.article_3_colonnes_savoir_plus{
display:inline-flex;
align-self: end;
justify-content: center;
text-align:center;
margin:0;
padding:24px;
width:~\"calc(100% - 48px)\";
text-align:center;
font-weight:600;
transition: background-color 400ms;
}
a.article_3_colonnes_savoir_plus:hover{
text-decoration:none;
}
}
}
}
}";
	$p1='Derniers articles';
	$p2='#fff';
	$p3='6';
	$p4='Lire le contenu';
	$p5='#fff';
	$p6=$code_couleur;
	$p7='20';
	$p8=$pourcentage_degrade_couleur;
	$p9=$css;
	// Création de l'entrée noisette
	sql_insertq('spip_spipr_educ',array(
		'nom'=>'sommaire_derniers_articles_3_colonnes',
		'type'=>'bloc de base',
		'nom_sauvegarde'=>'en_cours_d_utilisation_SPIPr',
		'parametre1'=>'sommaire',
		'parametre2'=>'off',
		'parametre3'=>'1',
		'parametre4'=>'cfg',
	));
	// Création de l'entrée graphique
	sql_insertq('spip_spipr_educ',array(
		'nom'=>'sommaire_derniers_articles_3_colonnes',
		'type'=>'graphisme',
		'nom_sauvegarde'=>'en_cours_d_utilisation_SPIPr',
		'parametre1'=>$p1,
		'parametre2'=>$p2,
		'parametre3'=>$p3,
		'parametre4'=>$p4,
		'parametre5'=>$p5,
		'parametre6'=>$p6,
		'parametre7'=>$p7,
		'parametre8'=>$p8,
		'parametre9'=>$p9,
	));
}
// Même travail avec le Carousel A la Une en version Slick
$test_sommaire_carousel_slick=sql_select('nom','spip_spipr_educ',"type='bloc de base' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND parametre1='sommaire' AND nom='sommaire_carousel_slick'");
$tab_sommaire_carousel_slick=sql_fetch($test_sommaire_carousel_slick);
if ($tab_sommaire_carousel_slick['nom']!='sommaire_carousel_slick') {
	// Recherche de la couleur actuelle et l'ensemble des paramètres
	include_spip('inc/spipr_educ_definitions_themes');
	$test_couleur=sql_select('parametre1','spip_spipr_educ',"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'");
	$tab_couleur=sql_fetch($test_couleur);
	$couleur=$tab_couleur['parametre1'];
	$retour_couleur=spipr_educ_definition_couleurs_theme_educnat($couleur);
	$code_couleur=$retour_couleur[0];
	$css="#sommaire_carousel_stick {
margin-bottom:50px;
.carousel_slick {
@media (min-width:768px) {padding:0 50px;}
.slick-prev, .slick-next {
text-shadow: 0 0 5px #555 !important;
height:30px !important;
width:30px !important;
@media (max-width:767px) {top:40% !important;}
}
.slick-next .slick-next-icon, .slick-next .slick-prev-icon, .slick-prev .slick-next-icon, .slick-prev .slick-prev-icon {
opacity: 0.9 !important;
}
.slick-next .slick-next-icon::before, .slick-next .slick-prev-icon::before, .slick-prev .slick-next-icon::before, .slick-prev .slick-prev-icon::before {
font-size: 30px !important;
}
.slick-autoplay-toggle-button:focus, .slick-dots li button:focus .slick-dot-icon:before,  .slick-dots li.slick-active button:focus .slick-dot-icon, .slick-autoplay-toggle-button:focus {color: #228BD0 !important;}
.sommaire_carousel_stick_container {
@media (min-width:768px) {
display:block;
width:724px;
height:auto;
margin:0;
padding:0;
}
.carousel_slick_logo {
@media (min-width:768px) {
display:block;
width:644px;
height:322px;
margin:0 40px;
}
a {
width:100%;
img{
width:100%;
height:auto;
}
}
}
.carousel_slick_titre {
@media (min-width:768px) {margin:0 40px;}
h3 {
margin:10px 0;
a {
padding:0;
margin:0;
font-family:'Archive', Arial, Helvetica, sans-serif;
}
}
}
.carousel_slick_intro {
@media (min-width:768px) {margin:6px 40px 30px 40px;}
a {color:#333;}
}
}
}
}";
	$p5='À la Une';
	$p6=$code_couleur;
	$p7='#333';
	$p8=$css;
	// Création de l'entrée noisette
	sql_insertq('spip_spipr_educ',array(
		'nom'=>'sommaire_carousel_slick',
		'type'=>'bloc de base',
		'nom_sauvegarde'=>'en_cours_d_utilisation_SPIPr',
		'parametre1'=>'sommaire',
		'parametre2'=>'off',
		'parametre3'=>'1',
		'parametre4'=>'cfg',
		'parametre5'=>$p5,
		'parametre6'=>$p6,
		'parametre7'=>$p7,
		'parametre8'=>$p8,
	));
}