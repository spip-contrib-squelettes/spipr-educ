<?php
// Définition des divers thèmes proposés
function spipr_educ_definition_themes() {
	$def = array(
		'theme_de_base',
		'terra',
		'spipr_institution',
		'educnat',
	);
	return $def;	
}

// Pour chaque thème, on définit les déclinaisons de couleurs qui sont proposées
function spipr_educ_defintion_couleurs($theme) {
	switch ($theme) {
    case 'theme_de_base':
        $retour = array(
			'bleu_clair','IndianRed','LightCoral','Salmon','Red','FireBrick','Maroon','DeepPink','MediumVioletRed','OrangeRed','BlueViolet','DarkOrchid','Purple','Indigo','SlateBlue','DarkSlateBlue','Lime','LimeGreen','SeaGreen','ForestGreen','Green','DarkGreen','OliveDrab','Olive','Teal','SteelBlue','MediumSlateBlue','Blue','DarkBlue','Navy','RosyBrown','DarkGoldenrod','Peru','Chocolate','SaddleBrown','Sienna','SlateGray'
		);
        break;
	case 'educnat':
		$retour = array('educnat_olive','terra_sapin','terra_amande','terra_vert_imperial','terra_verone','terra_verdure','terra_orange','terra_abricot','terra_carotte','terra_saumon','terra_red','terra_tomato','terra_cramoisi','terra_carmin','terra_pourpre','terra_framboise','terra_fuschia','educnat_rose','terra_orchidee','terra_amethyste','terra_darkmagenta','terra_indigo','terra_majorelle','educnat_bleu','educnat_celtic','terra_smalt','terra_sarcelle','terra_azur','terra_marine','terra_bleu_nuit','terra_barbeau','terra_bleu_ardoise','terra_ombre','terra_cannelle','terra_bronze');
		break;
    case 'terra':
	case 'spipr_institution':
        $retour = array('terra_olive','terra_sapin','terra_amande','terra_vert_imperial','terra_verone','terra_verdure','terra_lime','terra_orange','terra_abricot','terra_carotte','terra_saumon','terra_red','terra_tomato','terra_cramoisi','terra_carmin','terra_pourpre','terra_framboise','terra_fuschia','terra_orchidee','terra_amethyste','terra_darkmagenta','terra_indigo','terra_majorelle','terra_smalt','terra_sarcelle','terra_azur','terra_marine','terra_bleu_nuit','terra_barbeau','terra_bleu_ardoise','terra_ombre','terra_cannelle','terra_bronze');
        break;
	}
	return $retour;
}

// Permet de reprendre une couleur de base lorsqu'on change d'habillage
function spipr_educ_reinitialiser_theme($theme) {
	switch ($theme) {
		case 'educnat':
			spipr_educ_modif_couleur_theme('educnat','educnat_celtic');
		break;
		case 'theme_de_base' :
			spipr_educ_modif_couleur_theme('theme_de_base','bleu_clair');
		break;
		case 'terra' :
			spipr_educ_modif_couleur_theme('terra','terra_olive');
		break;
		case 'spipr_institution' :
			spipr_educ_modif_couleur_theme('spipr_institution','terra_olive');
		break;
	}
	if($theme!='educnat') {include_spip('inc/spipr_educ_educnat_retirer_menu_3_colonnes');}
	else {include_spip('inc/spipr_educ_educnat_ajouter_menu_3_colonnes');}
}

// Pour le thème de base, en fonction de la déclinaison de couleurs, on définit le jeu de couleur utile
function spipr_educ_definition_couleurs_theme_de_base($theme_de_couleur) {
	switch ($theme_de_couleur) {
		case 'bleu_clair' :
			// base, base foncée, puis gris (du plus clair au plus fonce)
			$retour = array('#0088cc','#005580');
		break;
		case 'IndianRed' :
			$retour = array('#CD5C5C','#9D3131');
		break;
		case 'LightCoral' :
			$retour = array('#F08080','#C01616');
		break;
		case 'Salmon' :
			$retour = array('#FA8072','#B91806');
		break;
		case 'Red' :
			$retour = array('#FF0000','#910000');
		break;
		case 'FireBrick' :
			$retour = array('#B22222','#7D1717');
		break;
		case 'Maroon' :
			$retour = array('#800000','#550000');
		break;
		case 'DeepPink' :
			$retour = array('#ff1493','#B90066');
		break;
		case 'MediumVioletRed' :
			$retour = array('#c71585','#910F60');
		break;
		case 'OrangeRed' :
			$retour = array('#FF4500','#B33100');
		break;
		case 'BlueViolet' :
			$retour = array('#8A2BE2','#441074');
		break;
		case 'DarkOrchid' :
			$retour = array('#9932CC','#591D76');
		break;
		case 'Purple' :
			$retour = array('#800080','#5E005E');
		break;
		case 'Indigo' :
			$retour = array('#4B0082','#3A0066');
		break;
		case 'SlateBlue' :
			$retour = array('#6A5ACD','#4332A3');
		break;
		case 'DarkSlateBlue' :
			$retour = array('#483D8B','#362E69');
		break;
		case 'Lime' :
			$retour = array('#00FF00','#00A400');
		break;
		case 'LimeGreen' :
			$retour = array('#32CD32','#269B26');
		break;
		case 'SeaGreen' :
			$retour = array('#2E8B57','#267549');
		break;
		case 'ForestGreen' :
			$retour = array('#228B22','#1A681A');
		break;
		case 'Green' :
			$retour = array('#008000','#006400');
		break;
		case 'DarkGreen' :
			$retour = array('#006400','#008000');
		break;
		case 'OliveDrab' :
			$retour = array('#6B8E23','#4E661A');
		break;
		case 'Olive' :
			$retour = array('#808000','#626200');
		break;
		case 'Teal' :
			$retour = array('#008080','#004F4F');
		break;
		case 'SteelBlue' :
			$retour = array('#4682B4','#2F5879');
		break;
		case 'MediumSlateBlue' :
			$retour = array('#7B68EE','#3317D0');
		break;
		case 'Blue' :
			$retour = array('#0000FF','#000082');
		break;
		case 'DarkBlue' :
			$retour = array('#00008B','#00005B');
		break;
		case 'Navy' :
			$retour = array('#000080','#0000D9');
		break;
		case 'RosyBrown' :
			$retour = array('#BC8F8F','#6A4040');
		break;
		case 'DarkGoldenrod' :
			$retour = array('#B8860B','#815C07');
		break;
		case 'Peru' :
			$retour = array('#CD853F','#875523');
		break;
		case 'Chocolate' :
			$retour = array('#D2691E','#793E11');
		break;
		case 'SaddleBrown' :
			$retour = array('#8B4513','#592E0D');
		break;
		case 'Sienna' :
			$retour = array('#A0522D','#733A20');
		break;
		case 'SlateGray' :
			$retour = array('#708090','#465059');
		break;
	}
	return $retour;
}

// Pour le thème de terra, en fonction de la déclinaison de couleurs, on définit le jeu de couleur utile
function spipr_educ_definition_couleurs_theme_terra($theme_de_couleur) {
	switch ($theme_de_couleur) {
		case 'terra_olive' :
			// base, base foncée, une couleur en complément, la même tirée vers le très clair, et pour le thème institution le pourcentage d'éclaircissement pour les dégradés
			$retour = array('#447777','#669999','#999966','#f1f1d9','55%');
		break;
		case 'terra_sapin' :
			$retour = array('#095228','#074220','#3d1e3a','#F9F2F8','80%');
		break;
		case 'terra_amande' :
			$retour = array('#82c46c','#60ae46','#7d3b93','#F8F1FA','50%');
		break;
		case 'terra_vert_imperial' :
			$retour = array('#00561b','#004516','#381a3c','#F8F0F9','80%');
		break;
		case 'terra_verone' :
			$retour = array('#5a6521','#48511a','#2f3357','#F1F2F8','65%');
		break;
		case 'terra_verdure' :
			$retour = array('#009966','#00714D','#AAAA2B','#F2F2CC','65%');
		break;
		case 'terra_lime' :
			$retour = array('#00ff00','#00cc00','#92f200','#FAFFF2','46%');
		break;
		case 'terra_orange' :
			$retour = array('#ff7f00','#cc6600','#5096C9','#ECF3F9','45%');
		break;
		case 'terra_abricot' :
			$retour = array('#e67e30','#c66318','#498FCF','#EBF2FA','40%');
		break;
		case 'terra_carotte' :
			$retour = array('#f4661b','#cf4e0a','#5A8ECD','#E7EEF8','40%');
		break;
		case 'terra_saumon' :
			$retour = array('#f88e55','#f56315','#b0bec5','#E9EDEF','30%');
		break;
		case 'terra_red' :
			$retour = array('#ff0000','#cc0000','#00aaff','#F0FAFF','40%');
		break;
		case 'terra_tomato' :
			$retour = array('#de2916','#b22112','#ba7784','#F9F2F3','50%');
		break;
		case 'terra_cramoisi' :
			$retour = array('#dc143c','#b01030','#a09cb0','#EDEDF1','50%');
		break;
		case 'terra_carmin' :
			$retour = array('#990000','#6C0000','#666600','#FFFFB7','67%');
		break;
		case 'terra_pourpre' :
			$retour = array('#9e0e40','#7e0b33','#579896','#EBF3F3','62%');
		break;
		case 'terra_framboise' :
			$retour = array('#c72c48','#9f233a','#bb757d','#F8EFF0','48%');
		break;
		case 'terra_fuschia' :
			$retour = array('#ff00ff','#cc00cc','#ce29ff','#FBECFF','48%');
		break;
		case 'terra_orchidee' :
			$retour = array('#da70d6','#cc3cc7','#63C788','#E8F7ED','32%');
		break;
		case 'terra_amethyste' :
			$retour = array('#884da7','#6d3e86','#719e64','#EEF3ED','47%');
		break;
		case 'terra_darkmagenta' :
			$retour = array('#8b008b','#6f006f','#008b00','#ECFFEC','70%');
		break;
		case 'terra_indigo' :
			$retour = array('#2e006c','#20004A','#007acc','#ECF8FF','76%');
		break;
		case 'terra_majorelle' :
			$retour = array('#6050dc','#3121A3','#87C2C2','#F0F7F7','35%');
		break;
		case 'terra_smalt' :
			$retour = array('#003399','#00297a','#E89C00','#FFF4DF','65%');
		break;
		case 'terra_sarcelle' :
			$retour = array('#008080','#006666','#522e31','#F8EFEF','70%');
		break;
		case 'terra_azur' :
			$retour = array('#007fff','#0066cc','#B1B744','#F3F4E3','43%');
		break;
		case 'terra_marine' :
			$retour = array('#03224c','#021b3d','#4f4f00','#FFFFE6','80%');
		break;
		case 'terra_bleu_nuit' :
			$retour = array('#0f056b','#0c0456','#304f35','#F0F7F1','75%');
		break;
		case 'terra_barbeau' :
			$retour = array('#5472ae','#425b8c','#a89f62','#F4F3EC','42%');
		break;
		case 'terra_bleu_ardoise' :
			$retour = array('#708090','#596673','#8f8271','#F1EEED','44%');
		break;
		case 'terra_ombre' :
			$retour = array('#926D27','#6D511D','#72BC58','#ECF5E7','60%');
		break;
		case 'terra_cannelle' :
			$retour = array('#7e5835','#65462a','#5a7283','#F1F3F5','60%');
		break;
		case 'terra_bronze' :
			$retour = array('#614e1a','#4e3e15','#2f4861','#F2F5F9','69%');
		break;
	}
	return $retour;
}
// Pour le thème Education Nationale 2020, en fonction de la déclinaison de couleurs, on définit le jeu de couleur utile
function spipr_educ_definition_couleurs_theme_educnat($theme_de_couleur) {
	switch ($theme_de_couleur) {
		case 'educnat_celtic' :
			// base, suivi des pourcentages pour dégradés
			$retour = array('#228BD0','70%','50%','10%','3%','17%');
		break;
		case 'educnat_olive' :
			$retour = array('#447777','70%','50%','10%','3%','17%');
		break;
		case 'terra_sapin' :
			$retour = array('#095228','70%','40%','10%','3%','17%');
		break;
		case 'terra_amande' :
			$retour = array('#82c46c','70%','50%','10%','3%','17%');
		break;
		case 'terra_vert_imperial' :
			$retour = array('#00561b','70%','50%','10%','3%','17%');
		break;
		case 'terra_verone' :
			$retour = array('#5a6521','70%','50%','10%','3%','17%');
		break;
		case 'terra_verdure' :
			$retour = array('#009966','70%','50%','10%','3%','17%');
		break;
		case 'terra_orange' :
			$retour = array('#ff7f00','70%','50%','10%','3%','17%');
		break;
		case 'terra_abricot' :
			$retour = array('#e67e30','70%','50%','10%','3%','17%');
		break;
		case 'terra_carotte' :
			$retour = array('#f4661b','70%','50%','10%','3%','17%');
		break;
		case 'terra_saumon' :
			$retour = array('#f88e55','70%','50%','10%','3%','17%');
		break;
		case 'terra_red' :
			$retour = array('#ff0000','70%','50%','10%','3%','17%');
		break;
		case 'terra_tomato' :
			$retour = array('#de2916','70%','50%','10%','3%','17%');
		break;
		case 'terra_cramoisi' :
			$retour = array('#dc143c','70%','50%','10%','3%','17%');
		break;
		case 'terra_carmin' :
			$retour = array('#990000','70%','50%','10%','3%','17%');
		break;
		case 'terra_pourpre' :
			$retour = array('#9e0e40','70%','50%','10%','3%','17%');
		break;
		case 'terra_framboise' :
			$retour = array('#c72c48','70%','50%','10%','3%','17%');
		break;
		case 'educnat_rose' :
			$retour = array('#ea5178','70%','50%','10%','3%','17%');
		break;
		case 'terra_fuschia' :
			$retour = array('#ff00ff','70%','50%','10%','3%','17%');
		break;
		case 'terra_orchidee' :
			$retour = array('#da70d6','70%','50%','10%','3%','17%');
		break;
		case 'terra_amethyste' :
			$retour = array('#884da7','70%','50%','10%','3%','17%');
		break;
		case 'terra_darkmagenta' :
			$retour = array('#8b008b','70%','50%','10%','3%','17%');
		break;
		case 'terra_indigo' :
			$retour = array('#2e006c','70%','50%','10%','3%','17%');
		break;
		case 'terra_majorelle' :
			$retour = array('#6050dc','70%','50%','10%','3%','17%');
		break;
		case 'terra_smalt' :
			$retour = array('#003399','70%','50%','10%','3%','17%');
		break;
		case 'educnat_bleu' :
			$retour = array('#2195dd','70%','50%','10%','3%','17%');
		break;
		case 'terra_sarcelle' :
			$retour = array('#008080','70%','50%','10%','3%','17%');
		break;
		case 'terra_azur' :
			$retour = array('#007fff','70%','50%','10%','3%','17%');
		break;
		case 'terra_marine' :
			$retour = array('#164092','70%','50%','10%','3%','17%');
		break;
		case 'terra_bleu_nuit' :
			$retour = array('#0f056b','70%','50%','10%','3%','17%');
		break;
		case 'terra_barbeau' :
			$retour = array('#5472ae','70%','50%','10%','3%','17%');
		break;
		case 'terra_bleu_ardoise' :
			$retour = array('#708090','70%','50%','10%','3%','17%');
		break;
		case 'terra_ombre' :
			$retour = array('#926D27','70%','50%','10%','3%','17%');
		break;
		case 'terra_cannelle' :
			$retour = array('#7e5835','70%','50%','10%','3%','17%');
		break;
		case 'terra_bronze' :
			$retour = array('#614e1a','70%','50%','10%','3%','17%');
		break;		
	}
	return $retour;
}

// Ici les appels SQL permettant d'adapter les entrées de la BDD lorsqu'on choisit une déclinaison de couleurs dans un thème et un jeu de couleurs choisis
function spipr_educ_modif_couleur_theme($theme,$theme_de_couleur) {
	switch ($theme) {
		case 'theme_de_base' :
			$couleurs_du_theme = spipr_educ_definition_couleurs_theme_de_base($theme_de_couleur);
			$base = $couleurs_du_theme[0];
			$base_foncee = $couleurs_du_theme[1];
			$gris0 = '#ffffff';
			$gris1 = '#eeeeee';
			$gris2 = '#dddddd';
			$gris3 = '#bbbbbb';
			$gris4 = '#aaaaaa';
			$gris5 ='#333333';
				// Couleur des liens du fil Twitter de la page de sommaire
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => $base,
					),
					"type='bloc de base' AND nom='sommaire_twitter' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleur des textes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris5,'parametre2' => $base,'parametre3' => $base_foncee,'parametre4' => $base,'parametre5' => $gris5,'parametre6' => $gris5,'parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_couleur_textes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Carousel / Une
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris1 ,'parametre2' => $gris5,'parametre3' => $gris2,'parametre4' => $base,'parametre5' => $gris0,'parametre6' => $gris3,'parametre7' => $gris4,'parametre8' => $gris3,'parametre9' => $base,'parametre10' => $gris0,
					),
					"type='graphisme' AND nom='graphisme_carousel' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris2,'parametre2' => $base,'parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_carousel_2' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Editorial
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris1,'parametre2' => $base,'parametre3' => $gris5,'parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_editorial_hero' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Typographie
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '14','parametre6' => '20','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_typographie' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Tableaux
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => 'non','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_tableaux' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Paginations
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => 'right','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_pagination' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Blocs dyslexie
				$noms_blocs_dys = array('sommaire_dyslexie','article_dyslexie','rubrique_dyslexie','breve_dyslexie','site_dyslexie','auteur_dyslexie','autre_dyslexie');
				foreach ($noms_blocs_dys as $bloc_dys) {
					sql_updateq(
						'spip_spipr_educ',
						array(
							'parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '',
						),
						"type='bloc de base' AND nom='$bloc_dys' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
					);
				}
				// Bloc des logos
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '',
					),
					"type='graphisme' AND nom='graphisme_bloc_logos' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Bloc des liens vers les partenaires
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_liens_partenaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu horizontal : icônes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'oui','parametre2' => 'non','parametre3' => 'non','parametre4' => 'non','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_icones' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu déroulant
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => $base,'parametre6' => '','parametre7' => '','parametre8' => $base,'parametre9' => $base,'parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_deroulant' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				
				// Marges
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'non',
					),
					"type='graphisme' AND nom='graphisme_marges' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Ensemble des entrées dont les 10 paramètres sont vidés
				$a_vider = array(
					'graphisme_formulaires', // Formulaires
					// Les blocs perso
					'autre_bloc_perso_1','autre_bloc_perso_2','autre_bloc_perso_3',
					'auteur_bloc_perso_1','auteur_bloc_perso_2','auteur_bloc_perso_3',
					'site_bloc_perso_1','site_bloc_perso_2','site_bloc_perso_3',
					'breve_bloc_perso_1','breve_bloc_perso_2','breve_bloc_perso_3',
					'rubrique_bloc_perso_1','rubrique_bloc_perso_2','rubrique_bloc_perso_3',
					'article_bloc_perso_1','article_bloc_perso_2','article_bloc_perso_3',
					'sommaire_bloc_perso_1','sommaire_bloc_perso_2','sommaire_bloc_perso_3',
					'graphisme_fil_ariane', //fil d'ariane
					'graphisme_css', // CSS
					'graphisme_couleur_fond', // Couleurs des fonds
					'graphisme_menu_horizontal', // Menu horizontal
					'graphisme_menu_vertical', // Menu vertical
				);	
				foreach ($a_vider as $nom_bloc) {
					sql_updateq(
						'spip_spipr_educ',
						array(
							'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
						),
						"type='graphisme' AND nom='$nom_bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
					);
				}
				
				// Up du nom et de la déclinaison du thème $theme,$theme_de_couleur
				sql_updateq(
					'spip_spipr_educ',
					array(
						'nom' => $theme,'parametre1' => $theme_de_couleur,'parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				$retour = 'ok';
			break;

		case 'terra' :
			$couleurs_du_theme=spipr_educ_definition_couleurs_theme_terra($theme_de_couleur);
			$couleur1 = $couleurs_du_theme[0];
			$couleur1bis = $couleurs_du_theme[1];
			$couleur2 = $couleurs_du_theme[2];
			$couleur2bis = $couleurs_du_theme[3];
			$gris0 = '#ffffff';
			$gris1 = '#f9f9f9';
			$gris2 = '#e8e8e8';
			$gris3 = '#e1e1e1';
			$gris4 = '#dddddd';
			$gris5 = '#cccccc';
			$gris6 = '#bbbbbb';
			$gris7 = '#aaaaaa';
			$gris8 = '#777777';
			$gris9 = '#666666';
			$gris10 = '#555555';
				// Couleur des liens du fil Twitter de la page de sommaire
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => $couleur1,
					),
					"type='bloc de base' AND nom='sommaire_twitter' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleur des textes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris9,'parametre2' => $couleur1bis,'parametre3' => $couleur1,'parametre4' => $couleur1,'parametre5' => $couleur2,'parametre6' => $couleur2,'parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_couleur_textes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Carousel / Une
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris1 ,'parametre2' => $gris10,'parametre3' => $couleur1,'parametre4' => $couleur1,'parametre5' => $gris0,'parametre6' => $gris6,'parametre7' => $gris7,'parametre8' => $gris6,'parametre9' => $couleur1,'parametre10' => $gris0,
					),
					"type='graphisme' AND nom='graphisme_carousel' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris4,'parametre2' => $couleur1,'parametre3' => '',
					),
					"type='graphisme' AND nom='graphisme_carousel_2' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Editorial
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris1,'parametre2' => $couleur2,'parametre3' => $gris10,'parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_editorial_hero' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Typographie
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '14','parametre6' => '20','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_typographie' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleurs des fonds
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_couleur_fond' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				
				// Tableaux
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris1,'parametre3' => $gris1,'parametre4' => 'oui','parametre5' => $gris4,'parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_tableaux' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Paginations
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris1,'parametre3' => $gris4,'parametre4' => 'right','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_pagination' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Blocs dyslexie
				$noms_blocs_dys = array('sommaire_dyslexie','article_dyslexie','rubrique_dyslexie','breve_dyslexie','site_dyslexie','auteur_dyslexie','autre_dyslexie');
				foreach ($noms_blocs_dys as $bloc_dys) {
					sql_updateq(
						'spip_spipr_educ',
						array(
							'parametre5' => $gris1,'parametre6' => '','parametre7' => $gris0,'parametre8' => 'box-shadow:0 0 10px '.$gris7.'; padding:10px;',
						),
						"type='bloc de base' AND nom='$bloc_dys' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
					);
				}
				// Bloc des logos
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => $couleur1,'parametre7' => $gris0,'parametre8' => '','parametre9' => 'padding:10px; box-shadow: 0px 0px 6px '.$gris7.';',
					),
					"type='graphisme' AND nom='graphisme_bloc_logos' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Bloc des liens vers les partenaires
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre2' => $gris0,'parametre3' => $gris2,'parametre4' => '','parametre5' => $couleur1bis,'parametre6' => $couleur1,'parametre7' => '','parametre9' => $gris3,'parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_liens_partenaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu vertical
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $couleur2bis,'parametre3' => $gris0,'parametre4' => $couleur1,'parametre5' => $gris0,'parametre6' => $couleur1bis,'parametre7' => $gris0,'parametre8' => $couleur1,'parametre9' => 'box-shadow : 2px 2px 6px '.$gris5.';','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_vertical' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu horizontal
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $couleur1,'parametre2' => $couleur1,'parametre3' => $couleur1,'parametre4' => $gris0,'parametre5' => $gris0,'parametre6' => $gris0,'parametre7' => $gris0,'parametre8' => $couleur1bis,'parametre9' => $couleur1bis,'parametre10' => '40',
					),
					"type='graphisme' AND nom='graphisme_menu_horizontal' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu horizontal : icônes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'non','parametre2' => 'non','parametre3' => 'non','parametre4' => 'non','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_icones' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu déroulant
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris7,'parametre3' => $gris2,'parametre4' => $gris0,'parametre5' => $couleur1,'parametre6' => $gris0,'parametre7' => $gris0,'parametre8' => $couleur1bis,'parametre9' => $couleur1,'parametre10' => 'non',
					),
					"type='graphisme' AND nom='graphisme_menu_deroulant' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Formulaires
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => 'box-shadow:0 0 8px '.$gris8.';',
					),
					"type='graphisme' AND nom='graphisme_formulaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);				
				// Marges
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'oui',
					),
					"type='graphisme' AND nom='graphisme_marges' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Fil d'ariane
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $couleur2bis,'parametre2' => $gris0,'parametre3' => $couleur1bis,'parametre4' => $couleur1,'parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_fil_ariane' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// CSS
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '.container {box-shadow:6px 0 6px #CCC,-6px 0 6px #CCC;} #nav .menu {border:none;}','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_css' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Ensemble des entrées dont les 10 paramètres sont vidés
				$a_vider = array(
					// Les blocs perso
					'autre_bloc_perso_1','autre_bloc_perso_2','autre_bloc_perso_3',
					'auteur_bloc_perso_1','auteur_bloc_perso_2','auteur_bloc_perso_3',
					'site_bloc_perso_1','site_bloc_perso_2','site_bloc_perso_3',
					'breve_bloc_perso_1','breve_bloc_perso_2','breve_bloc_perso_3',
					'rubrique_bloc_perso_1','rubrique_bloc_perso_2','rubrique_bloc_perso_3',
					'article_bloc_perso_1','article_bloc_perso_2','article_bloc_perso_3',
					'sommaire_bloc_perso_1','sommaire_bloc_perso_2','sommaire_bloc_perso_3',
					
				);	
				foreach ($a_vider as $nom_bloc) {
					sql_updateq(
						'spip_spipr_educ',
						array(
							'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
						),
						"type='graphisme' AND nom='$nom_bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
					);
				}
				
				// Up du nom et de la déclinaison du thème $theme,$theme_de_couleur
				sql_updateq(
					'spip_spipr_educ',
					array(
						'nom' => $theme,'parametre1' => $theme_de_couleur,'parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				$retour = 'ok';
			break;
			
			case 'spipr_institution' :
			$couleurs_du_theme = spipr_educ_definition_couleurs_theme_terra($theme_de_couleur);
			$couleur1 = $couleurs_du_theme[0];
			$couleur1bis = $couleurs_du_theme[1];
			$couleur2 = $couleurs_du_theme[2];
			$couleur2bis = $couleurs_du_theme[3];
			$degrade = $couleurs_du_theme[4];
			$gris0 = '#ffffff';
			$gris1 = '#f9f9f9';
			$gris2 = '#e8e8e8';
			$gris3 = '#e1e1e1';
			$gris4 = '#dddddd';
			$gris5 = '#cccccc';
			$gris6 = '#bbbbbb';
			$gris7 = '#aaaaaa';
			$gris78 = '#999999';
			$gris8 = '#777777';
			$gris9 = '#666666';
			$gris10 = '#555555';
				// Couleur des liens du fil Twitter de la page de sommaire
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => $couleur1,
					),
					"type='bloc de base' AND nom='sommaire_twitter' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleur des textes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris9,'parametre2' => $couleur1bis,'parametre3' => $couleur1,'parametre4' => $couleur1,'parametre5' => $couleur2,'parametre6' => $couleur2,'parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_couleur_textes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Carousel / Une
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris1 ,'parametre2' => $gris10,'parametre3' => $couleur1,'parametre4' => $couleur1,'parametre5' => $gris0,'parametre6' => $gris6,'parametre7' => $gris7,'parametre8' => $gris6,'parametre9' => $couleur1,'parametre10' => $gris0,
					),
					"type='graphisme' AND nom='graphisme_carousel' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris4,'parametre2' => $couleur1,'parametre3' => '',
					),
					"type='graphisme' AND nom='graphisme_carousel_2' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Editorial
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris1,'parametre2' => $couleur2,'parametre3' => $gris10,'parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_editorial_hero' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Typographie
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '14','parametre6' => '20','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_typographie' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleurs des fonds
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_couleur_fond' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				
				// Tableaux
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris1,'parametre3' => $gris1,'parametre4' => 'oui','parametre5' => $gris4,'parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_tableaux' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Paginations
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris1,'parametre3' => $gris4,'parametre4' => 'right','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_pagination' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Blocs dyslexie
				$noms_blocs_dys = array('sommaire_dyslexie','article_dyslexie','rubrique_dyslexie','breve_dyslexie','site_dyslexie','auteur_dyslexie','autre_dyslexie');
				foreach ($noms_blocs_dys as $bloc_dys) {
					sql_updateq(
						'spip_spipr_educ',
						array(
							'parametre5' => $gris1,'parametre6' => '','parametre7' => $gris0,'parametre8' => 'box-shadow:0 0 10px '.$gris7.'; padding:10px;',
						),
						"type='bloc de base' AND nom='$bloc_dys' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
					);
				}
				// Bloc des logos
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => $couleur1,'parametre7' => $gris0,'parametre8' => '','parametre9' => 'padding:10px; box-shadow: 0px 0px 6px '.$gris7.';',
					),
					"type='graphisme' AND nom='graphisme_bloc_logos' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Bloc des liens vers les partenaires
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre2' => $gris0,'parametre3' => $gris2,'parametre4' => '','parametre5' => $couleur1bis,'parametre6' => $couleur1,'parametre7' => '','parametre9' => $gris3,'parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_liens_partenaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu vertical
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $couleur2bis,'parametre3' => $gris0,'parametre4' => $couleur1,'parametre5' => $gris0,'parametre6' => $couleur1bis,'parametre7' => $gris0,'parametre8' => $couleur1,'parametre9' => 'box-shadow : 2px 2px 6px '.$gris5.';','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_vertical' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu horizontal
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $couleur1,'parametre2' => $couleur1,'parametre3' => $couleur1,'parametre4' => $gris0,'parametre5' => $gris0,'parametre6' => $gris0,'parametre7' => $gris0,'parametre8' => $couleur1bis,'parametre9' => $couleur1bis,'parametre10' => '40',
					),
					"type='graphisme' AND nom='graphisme_menu_horizontal' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu horizontal : icônes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'non','parametre2' => 'non','parametre3' => 'non','parametre4' => 'non','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_icones' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu déroulant
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris7,'parametre3' => $gris2,'parametre4' => $gris0,'parametre5' => $couleur1,'parametre6' => $gris0,'parametre7' => $gris0,'parametre8' => $couleur1bis,'parametre9' => $couleur1,'parametre10' => 'non',
					),
					"type='graphisme' AND nom='graphisme_menu_deroulant' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Formulaires
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => 'box-shadow:0 0 8px '.$gris8.';',
					),
					"type='graphisme' AND nom='graphisme_formulaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);				
				// Marges
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'oui',
					),
					"type='graphisme' AND nom='graphisme_marges' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Fil d'ariane
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $couleur2bis,'parametre2' => $gris0,'parametre3' => $couleur1bis,'parametre4' => $couleur1,'parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_fil_ariane' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// CSS
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' =>						
'/* ---------------------------------------------- */
/* CSS spécifiques thème Académie Rouen */
/* ---------------------------------------------- */

/* -- Base CSS : ombre gauche et droite -- */
.container {box-shadow:6px 0 6px '.$gris5.',-6px 0 6px '.$gris5.';}
#nav .menu {border:none;}

/* -- CSS du pied de page -- */
#footer, #footer.footer {width:100%; padding:0; margin:0;}
#pied {position : relative; clear:both; display:block; background-image:linear-gradient('.$gris0.', lighten('.$couleur1.', '.$degrade.'));}
ul#pied  {margin: 0; padding :0; list-style:none;}
ul#pied li {padding:0; margin:0; text-indent:none; list-style-position:outside; list-style:none;}
ul#pied li.pied_cat {padding: 2% 3%; width:94%; background:none;}
ul#pied .pied_label {width:30%; text-transform:uppercase; display:inline-block; float:left;}
ul#pied ul.pied_liste {width:64%; display:inline-block; float:right;}
li.avec_separateur{border-bottom : 1px solid '.$gris10.';}
#pied li.pied_niv2 {float:left; padding:0 1%; margin: 0;}
#pied li.pied_niv2.avec_separateur_niv2 {border-right: 1px solid '.$gris10.';}
#pied li.pied_niv2, #pied li.pied_niv2 a {font-size : 92%; text-decoration:none;}
#pied li.ecart_gauche {padding-left:10%;}
.liste_DSDEN {padding-top:0.5em;}
#footer-plain {margin:0; padding:0px 2%; text-align:center; width:96%; background-color:'.$couleur1.'; color:'.$gris0.';}
#footer-plain li {padding:0.4% 1%; float:left; color :'.$gris0.'; list-style:none;}
#footer-plain li a {font-size:92%; color :'.$gris0.'; text-decoration:none;}

/* -- CSS entête -- */
header.accueil  h1#logo_site_spip img{position:absolute; left:0; top:0; max-width:200px; max-height:100px; margin:auto;}
header.accueil  h1#logo_site_spip #header_separation {position:absolute; left:234px; top:10px; width:2px; height:80px; background-color:'.$gris7.';}
header.accueil  h1#logo_site_spip #nom_site_spip {position:absolute; left:240px; top:35px;}
div#header div#liens_partenaires {margin-bottom:0;}

@media (min-width: 1200px) {
	#menu_racine {width:1140px;}
	header.accueil {margin:0; padding:0; top:0; left:0; width:1170px; height:120px;}
	header.accueil  h1#logo_site_spip {position:absolute; left:30px; top:10px; width: 1140px; height:100px;}
	header.accueil  h1#logo_site_spip #slogan_site_spip {position:absolute; right:30px; bottom:0; width:806px;}
	#header_recherche div#formulaire_recherche {position:absolute; right:20px; width:300px; top:45px; margin:0; padding:0;}
	#header_recherche div#formulaire_recherche.formulaire_spip.formulaire_recherche form div input#recherche {width:220px; margin:0;  margin-right:10px;}
	header.accueil  h1#logo_site_spip #nom_site_spip {width:520px; top:10px;}
}
@media (min-width: 980px) and (max-width: 1199px) {
	#menu_racine {width:880px;}
	header.accueil {margin:0; padding:0, top:0, left:0; width:940px; height:120px;}
	header.accueil  h1#logo_site_spip {position:absolute; left:30px; top:10px; width:910px; height:100px;}
	header.accueil  h1#logo_site_spip #slogan_site_spip {position:absolute; right:30px; bottom:0; width:576px;}
	#header_recherche div#formulaire_recherche {position:absolute; right:20px; width:240px; top:45px; margin:0; padding:0;}
	#header_recherche div#formulaire_recherche.formulaire_spip.formulaire_recherche form div input#recherche {width:160px; margin:0; margin-right:10px;}
	header.accueil  h1#logo_site_spip #nom_site_spip {width:360px; top:10px;}
}
@media (min-width: 768px) and(max-width: 979px) {
	#menu_racine {width:670px;}
	header.accueil {margin:0; padding:0, top:0, left:0; width:720px; height:120px;}
	header.accueil  h1#logo_site_spip {position:absolute; left:30px; top:10px; width:690px; height:100px;}
	header.accueil  h1#logo_site_spip #slogan_site_spip {position:absolute; text-align:right; right:30px; bottom:0; width:356px;  font-size:0.6em;}
	#header_recherche div#formulaire_recherche {position:absolute; right:20px; width:200px; top:10px; margin:0; padding:0;}
	#header_recherche div#formulaire_recherche.formulaire_spip.formulaire_recherche form div input#recherche {width:120px; margin:0; margin-right:10px;}
	header.accueil  h1#logo_site_spip #nom_site_spip {width:380px; top:25px;  font-size:0.9em;}
}
@media (max-width: 767px) {
	header.accueil {margin:0; padding:0, top:0, left:0; width:100%; height:120px;}
	header.accueil  h1#logo_site_spip {position:absolute; left:30px; top:10px; height:100px; width: ~"calc(100% - 60px)";}
	header.accueil  h1#logo_site_spip img {width:100px; height:50px; top:25px;}
	header.accueil  h1#logo_site_spip #nom_site_spip {position:absolute; left:100px; top:30px; width:~"calc(100% - 100px)"; font-size:0.75em;}
	header.accueil  h1#logo_site_spip #header_separation {display:none;}
	header.accueil  h1#logo_site_spip #slogan_site_spip {position:absolute; right:0; bottom:0; width:~"calc(100% - 100px)"; text-align:right; font-size:0.45em;}
	#header_recherche div#formulaire_recherche {position:absolute; right:0; width:220px; top:10px; margin:0; padding:0;}
	#header_recherche div#formulaire_recherche.formulaire_spip.formulaire_recherche form div input#recherche {width:120px; margin:0; margin-right:10px;}
}

@media (max-width: 500px) {
	header.accueil  h1#logo_site_spip #nom_site_spip {font-size:0.5em;}
	header.accueil  h1#logo_site_spip #slogan_site_spip {font-size:0.35em;}
}

#spipr_menu_langue{position: absolute; right:30px; bottom: 10px; width:200px;}

/* ------------------------------------------ */
/* --- Menu de navigation horizontal --- */
/* ------------------------------------------ */

/* -- Tailles items menu déplié -- */
@media (min-width: 1200px) {
	.plan_rubriques li.rub {width:275px;}
	.plan_rubriques ul.sous_rubriques_CE {width:265px;}
	.plan_rubriques li.rub span {width: 265px;}
}
@media (min-width: 980px) and (max-width:1199px) {
	.plan_rubriquesli.rub {width:290px;}
	.plan_rubriques ul.sous_rubriques_CE {width:280px;}
	.plan_rubriques li.rub span {width:280px;}
}
@media (min-width: 768px) and (max-width:979px){
	.plan_rubriques li.rub {width:220px;}
	.plan_rubriques ul.sous_rubriques_CE {width:210px;}
	.plan_rubriques li.rub span {width:210px;}
}

.caret {vertical-align:middle;}
#sous_menu{
	width: 100%;
	display:block;
	background:'.$gris0.';
}

.encart_menu_rub {
	width: 100%;
	position:relative;
	padding: 0px 0px 25px 0px;
	display:none;
	border-bottom : 2px solid '.$gris6.';
	min-height:0;
	background-image:linear-gradient('.$gris0.', lighten('.$couleur1.', '.$degrade.'));
}
.sous_rubrique_active {display:block;}
.plan_rubriques {
	margin-top:10px;
	float:left;
	width:100%;
}
.acces {
	position:absolute;
	bottom:5px;
	right:15px;
	font-size:11px;
	font-weight:normal;
	height:20px;
}
.acces a {text-decoration:none;}
.acces a.fermer_menu {
	color:'.$gris10.';
	background-color:'.$gris0.';
	border:1px solid '.$gris10.';
	padding:2px 5px;
	margin-left:10px;
}

.plan_rubriques li {margin:0; padding:0;}
.plan_rubriques ul {
	padding:0;
	margin:0;
	padding-left:15px;
	list-style-type:none;
}
.plan_rubriques li {margin:0; padding:0;}
.plan_rubriques li.rub {
	float:left;
	margin:0 0 10px 0;
	padding:2px 5px 5px;
}
.plan_rubriques ul.sous_rubriques_CE {
	float:none;
	padding:5px 5px 5px 10px;
	list-style-type:disc;
	list-style-position:inside;
}
.plan_rubriques ul.sous_rubriques_CE li {margin:0; padding:0;}

.plan_rubriques li.rub span {
	padding:0px 0px 0px 10px;
	display:block;
}
.plan_rubriques a {text-decoration:none;}
a.sous_rub_link {
	font-size:12px;
	font-weight:bold;
}
a.sous_sous_rub {color:'.$gris10.';}

/* ------------------------------------------ */
/* --------- Adaptations menus --------- */
/* ------------------------------------------ */
@media (min-width: 768px) {
	#menu_racine div.encart_menu_rub, #menu_racine .encart_menu_rub div.plan_rubriques, #menu_racine .encart_menu_rub div.acces {display:none;margin:0; padding:0; border:none; height:0; width:0; line-height:0;}
}
@media (max-width:767px){
	#sous_menu {display:none;}
	#menu_racine .encart_menu_rub div.plan_rubriques {
		margin:0;
		padding:10px 0;
		background-image:linear-gradient('.$gris0.', lighten('.$couleur1.', '.$degrade.'));
	}
	#menu_racine .encart_menu_rub div.acces {position:relative; text-align:right;}
}
/* ------------------------------------------ */
/* -------- Adaptations carousel -------- */
/* ------------------------------------------ */

#header .carousel {background-image:linear-gradient(#ffffff, lighten('.$couleur1.', '.$degrade.'));
border-left:none; border-right:none;}
#header h2.titre-du-carousel {display:none;}
#header .carousel strong a span.titre {font-weight:normal;}

@media (min-width: 1200px) {
	#header .carousel, #header .carousel .carousel-inner .item article  {height:279px;}
	.de_980_a_1200,.de_768_a_980,.moins_de_768 {display:none;}
	.carousel .carousel-stop {left:12%;}
	.carousel ol.carousel-indicators{left:16%;}
}

@media (min-width: 980px) and (max-width:1199px) {
	#header .carousel, #header .carousel .carousel-inner .item article  {height:228px;}
	#header .carousel .carousel-inner .item article span.img {width:555px; height:228px;}
	.plus_de_1200,.de_768_a_980,.moins_de_768 {display:none;}
	.carousel .carousel-stop {left:12%;}
	.carousel ol.carousel-indicators{left:16%;}
}

@media (min-width: 768px) and (max-width:980px) {
	#header .carousel, #header .carousel .carousel-inner .item article  {height:178px;}
	#header .carousel .carousel-inner .item article span.img {width:555px; height:178px;}
	.plus_de_1200,.de_980_a_1200,.moins_de_768 {display:none;}
	.carousel .carousel-stop {left:12%;}
	.carousel ol.carousel-indicators{left:16%;}
}
@media (max-width:767px) {
	#header .carousel .carousel-inner .item article  strong.moins_de_768 {height:350px; text-align:center;}
	#header .carousel .carousel-inner .item article  strong.moins_de_768  .logo_moins_de_768{
		display:block;
		position:relative;
		float:left;
		width:100%;
	max-height:150px;
	}
	#header .carousel .carousel-inner .item article  strong.moins_de_768  .logo_moins_de_768 img{
		text-align:center;
		max-height:150px;
	}
	#header .carousel .carousel-inner .item article  strong.moins_de_768  .titre_moins_de_768 {
		display:block;
		position:relative;
		width:100%;
		float:left;
		text-align:center; 
		min-height:50px;
	}
	.plus_de_1200,.de_980_a_1200,.de_768_a_980 {display:none;}
	.carousel .carousel-stop {left:42%;}
	.carousel ol.carousel-indicators{left:50%;}
	body {padding-left:0; padding-right:0;}
}

/* ------------------------------------------ */
/* ----- Modèle competences CRCN ---- */
/* ------------------------------------------ */

div.competences {
margin-bottom:30px;
padding:10px;
box-shadow:0 0 4px #888;
h4.h4 {font-size:1.2em;}
p.legifrance {
font-size:0.9em;
margin:10px 0 0 0;
}
}
',
					'parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
				),
				"type='graphisme' AND nom='graphisme_css' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
			);
			
			// Textes des formulaires plus visibles
			sql_updateq(
				'spip_spipr_educ',
				array(
					'parametre2' => $gris78,
				),
				"type='graphisme' AND nom='graphisme_formulaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
			);
			
			// Placer le carousel en bas de l'entête du sommaire
			sql_updateq(
				'spip_spipr_educ',
				array(
					'parametre2' => 'header','parametre3' => '999',
				),
				"type='bloc de base' AND nom='sommaire_carousel' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr' AND parametre1='sommaire'"
			);
			include_spip('inc/spipr_educ_deplacement_bloc');
			spipr_educ_bloc_ranger('sommaire','header');

			// Ensemble des entrées dont les 10 paramètres sont vidés
			$a_vider = array(
				// Les blocs perso
				'autre_bloc_perso_1','autre_bloc_perso_2','autre_bloc_perso_3',
				'auteur_bloc_perso_1','auteur_bloc_perso_2','auteur_bloc_perso_3',
				'site_bloc_perso_1','site_bloc_perso_2','site_bloc_perso_3',
				'breve_bloc_perso_1','breve_bloc_perso_2','breve_bloc_perso_3',
				'rubrique_bloc_perso_1','rubrique_bloc_perso_2','rubrique_bloc_perso_3',
				'article_bloc_perso_1','article_bloc_perso_2','article_bloc_perso_3',
				'sommaire_bloc_perso_1','sommaire_bloc_perso_2','sommaire_bloc_perso_3',
				
			);	
			foreach ($a_vider as $nom_bloc) {
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='$nom_bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
			}
			
			// Up du nom et de la déclinaison du thème $theme,$theme_de_couleur
			sql_updateq(
				'spip_spipr_educ',
				array(
					'nom' => $theme,'parametre1' => $theme_de_couleur,'parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
				),
				"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
			);
			$retour = 'ok';
		break;
		
		case 'educnat' :
			$couleurs_du_theme = spipr_educ_definition_couleurs_theme_educnat($theme_de_couleur);
			$couleur1 = $couleurs_du_theme[0];
			$couleur1degrade1 = $couleurs_du_theme[1];
			$couleur1degrade2 = $couleurs_du_theme[2];
			$couleur1degrade3 = $couleurs_du_theme[3];
			$couleur1degrade4 = $couleurs_du_theme[4];
			$couleur1degrade5 = $couleurs_du_theme[5];
			$gris0 = '#ffffff';
			$gris1 = '#f9f9f9';
			$gris12 = '#f3f3f3';
			$gris2 = '#eeeeee';
			$gris3 = '#e1e1e1';
			$gris4 = '#dddddd';
			$gris5 = '#cccccc';
			$gris6 = '#bbbbbb';
			$gris7 = '#aaaaaa';
			$gris78 = '#999999';
			$gris8 = '#777777';
			$gris9 = '#333333';
			$gris10 = '#000000';
				// Couleur des liens du fil Twitter de la page de sommaire
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => $couleur1,
					),
					"type='bloc de base' AND nom='sommaire_twitter' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleur de fond du bloc de présentation des rubriques en 3 colonnes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre5' => $couleur1,
					),
					"type='bloc de base' AND nom='rubrique_menu_rubriques_3_colonnes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Paramètres du bloc de présentation des derniers articles en 3 colonnes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre2' => '#fff',
						'parametre5' => '#fff',
						'parametre6' => $couleur1,
					),
					"type='graphisme' AND nom='sommaire_derniers_articles_3_colonnes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Paramètres du bloc de Carousel du sommaire utilisant Slick
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => $couleur1,
						'parametre7' => '#333',
					),
					"type='bloc de base' AND nom='sommaire_carousel_slick' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleur des textes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris9,'parametre2' => $couleur1,'parametre3' => 'darken('.$couleur1.', '.$couleur1degrade1.')','parametre4' => $couleur1,'parametre5' => $gris9,'parametre6' => $couleur1,'parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_couleur_textes' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Carousel / Une
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $couleur1 ,'parametre2' => $gris9,'parametre3' => $gris0,'parametre4' => $gris0,'parametre5' => $gris9,'parametre6' => $gris0,'parametre7' => $gris4,'parametre8' => $gris6,'parametre9' => $gris0,'parametre10' => $gris10,
					),
					"type='graphisme' AND nom='graphisme_carousel' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris2,'parametre2' => $gris0,'parametre3' => 'margin-bottom:50px;',
					),
					"type='graphisme' AND nom='graphisme_carousel_2' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Editorial
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris1,'parametre2' => $couleur1,'parametre3' => $gris9,'parametre4' => 'margin-bottom:50px;','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_editorial_hero' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Typographie
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '"Roboto",Arial,Helvetica,sans-serif','parametre2' => '"Roboto",Arial,Helvetica,sans-serif','parametre3' => '"Roboto",Arial,Helvetica,sans-serif','parametre4' => '"Roboto",Arial,Helvetica,sans-serif','parametre5' => '14','parametre6' => '20','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_typographie' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Couleurs des fonds
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_couleur_fond' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				
				// Tableaux
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => 'non','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_tableaux' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Paginations
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => 'right','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_pagination' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Blocs dyslexie
				$noms_blocs_dys = array('sommaire_dyslexie','article_dyslexie','rubrique_dyslexie','breve_dyslexie','site_dyslexie','auteur_dyslexie','autre_dyslexie');
				foreach ($noms_blocs_dys as $bloc_dys) {
					sql_updateq(
						'spip_spipr_educ',
						array(
							'parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '',
						),
						"type='bloc de base' AND nom='$bloc_dys' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
					);
				}
				// Bloc des logos
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => 'margin-bottom:50px;',
					),
					"type='graphisme' AND nom='graphisme_bloc_logos' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Bloc des liens vers les partenaires
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_liens_partenaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu vertical
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $couleur1,'parametre2' => '#fafafa','parametre3' => $gris78,'parametre4' => $couleur1,'parametre5' => 'darken('.$couleur1.','.$couleur1degrade1.')','parametre6' => '#f3f3f3','parametre7' => $gris0,'parametre8' => $couleur1,'parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_vertical' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu horizontal
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris0,'parametre3' => 'transparent','parametre4' => $gris10,'parametre5' => $gris10,'parametre6' => $gris10,'parametre7' => $gris10,'parametre8' => $gris0,'parametre9' => $gris0,'parametre10' => '40',
					),
					"type='graphisme' AND nom='graphisme_menu_horizontal' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu horizontal : icônes
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'non','parametre2' => 'non','parametre3' => 'non','parametre4' => 'non','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_menu_icones' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Menu déroulant
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => $gris0,'parametre2' => $gris7,'parametre3' => '#e5e5e5','parametre4' => $gris0,'parametre5' => $couleur1,'parametre6' => $gris0,'parametre7' => $gris0,'parametre8' => $couleur1,'parametre9' => $couleur1,'parametre10' => 'non',
					),
					"type='graphisme' AND nom='graphisme_menu_deroulant' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Formulaires
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '#f3f3f3','parametre2' => $gris9,'parametre3' => $couleur1,'parametre4' => '',
					),
					"type='graphisme' AND nom='graphisme_formulaires' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);				
				// Marges
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'non',
					),
					"type='graphisme' AND nom='graphisme_marges' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Fil d'ariane
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => 'transparent','parametre2' => 'transparent','parametre3' => $couleur1,'parametre4' => 'darken('.$couleur1.','.$couleur1degrade1.')','parametre5' => 'padding-left:0; a {background-color:transparent; color:darken('.$couleur1.','.$couleur1degrade1.'); font-weight:normal; } a:hover {color:'.$couleur1.'; background-color:transparent; font-weight:normal; text-decoration:underline;}','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='graphisme_fil_ariane' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// CRCN
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre5' => 'oui','parametre6' => '24','parametre7' => 'div.competences {margin-bottom:30px; padding:10px;	box-shadow:0 0 4px #888;	h4.h4 {font-size:1.2em;}	p.legifrance {	font-size:0.9em;	margin:10px 0 0 0;}}','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='bloc de base' AND nom='article_competences_crcn' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// Layouts particuliers pour ce thème
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '27','parametre2' => 'auto','parametre3' => '48%','parametre4' => '48%','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => ''
					),
					"type='layout' AND nom='layout_petit' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '39','parametre2' => '12','parametre3' => '6','parametre4' => '6','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => ''
					),
					"type='layout' AND nom='layout_moyen' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '39','parametre2' => '12','parametre3' => '6','parametre4' => '6','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => ''
					),
					"type='layout' AND nom='layout_grand' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '39','parametre2' => '12','parametre3' => '6','parametre4' => '6','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => ''
					),
					"type='layout' AND nom='layout_1200' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
				// CSS
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' =>
"
/* -- Polices spécifiques -- */
@font-face{
font-family:'Archive';
src:url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/archive-regular-webfont.woff2')
format('woff2'),url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/archive-regular-webfont.woff') format('woff');
font-weight:400;
font-style:normal;
}

@font-face{
font-family:'Roboto';
src:url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/roboto-regular-webfont.woff2') format('woff2'),url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/roboto-regular-webfont.woff') format('woff');
font-weight:400;
font-style:normal;
}

@font-face{
font-family:'Roboto';
src:url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/roboto-medium-webfont.woff2') format('woff2'),url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/roboto-medium-webfont.woff') format('woff');
font-weight:500;
font-style:normal;
}

@font-face{
font-family:'Roboto';
src:url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/roboto-bold-webfont.woff2') format('woff2'),url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/roboto-bold-webfont.woff') format('woff');
font-weight:700;
font-style:normal;
}

@font-face {
  font-family: 'icomoon';
  src:url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/icomoon.eot?8fpotj'); src:url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/icomoon.eot?8fpotj#iefix') format('embedded-opentype'),
url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/icomoon.ttf?8fpotj') format('truetype'),
url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/icomoon.woff?8fpotj') format('woff'),
url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/css/fonts/icomoon.svg?8fpotj#icomoon') format('svg');
  font-weight: normal;
  font-style: normal;
  font-display: block;
  line-height:1;
}

[class^='icon-'], [class*=' icon-'] {
  /* use !important to prevent issues with browser extensions that change fonts */
  font-family: 'icomoon' !important;
  speak: never;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;

  /* Better Font Rendering =========== */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

/* -- Style pour la page des articles, des rubriques -- */
#article_cartouche_container {
display:block;
position:relative;
background: linear-gradient(to right bottom, $couleur1, lighten($couleur1,$couleur1degrade2));
background-position: top left;
background-repeat: no-repeat;
background-attachment: scroll;
background-size: 100%;
width:100%;
margin:40px 0 50px 0;
box-shadow: inset 0 0 1200px 1200px rgba(0, 0, 0, .15);
#article_cartouche_titre{
display:block;
position: absolute;
width:auto;
height:auto;
h1 {
font-family:'Archive', Arial, Helvetica, sans-serif;
color:$gris0;
text-shadow: 0 0 1px $gris10;
font-weight:400;
}
}
#article_cartouche_chapeau {
display: block;
position: absolute;
background-color:rgba(0, 0, 0, 0.6);
color:$gris0;
height:auto;
overflow:hidden;
text-align:justify;
}
#article_cartouche_mots_cles{
display: block;
position: absolute;
ul {display:inline-block; margin:0; padding:0; list-type:none;
li {display:inline; margin-right:8px; margin-bottom:30px; color:$gris0;
a {white-space: nowrap; line-height: 46px;}
}
}
}
#container_socialtags:hover {background-color:rgba(255, 255, 255, 0.9);}
#container_socialtags {
display:block;
width:auto;
height:auto;
margin:0;
padding:12px;
position: absolute;
background-color: rgba(255, 255, 255, 0.7);
border: none;
box-shadow: 0 0 4px $gris7;
border-radius:4px;
}

@media (min-width: 1200px){
height:600px;
#container_socialtags {
top: 60px;
right: 80px;
}
#article_cartouche_titre{
top:60px; left:80px; min-width: 320px; max-width:520px;
h1 {
margin:30px 0;
font-size:32px;
line-height:42px;
}
h1::before{
content:'';
position:absolute;
width:100px;
height:14px;
top:0;
left:0;
border-left:7px solid $gris0;
border-bottom:7px solid $gris0;
}
h1::after{
content:'';
position:absolute;
width:300px;
height:14px;
bottom:4px;
right:0;
border-right:7px solid $gris0;
border-top:7px solid $gris0;
}
}
#article_cartouche_mots_cles{
left:80px;
top:500px;
width:700px;
ul li {
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
#article_cartouche_chapeau {
top: 280px;
left:0;
padding:20px 24px 16px 80px;
max-width:680px;
max-height:100px;
}
}

@media (min-width: 980px) and (max-width:1199px) {
height:490px;
#container_socialtags {
top: 50px;
right: 60px;
}
#article_cartouche_titre{
top:60px; left:60px; min-width: 320px; max-width:520px;
h1 {
margin:28px 0;
font-size:30px;
line-height:40px;
}
h1::before{
content:'';
position:absolute;
width:90px;
height:12px;
top:0;
left:0;
border-left:6px solid $gris0;
border-bottom:6px solid $gris0;
}
h1::after{
content:'';
position:absolute;
width:240px;
height:12px;
bottom:2px;
right:0;
border-right:6px solid $gris0;
border-top:6px solid $gris0;
}
}
#article_cartouche_mots_cles{
left:60px;
top:380px;
width:540px;
ul li {
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
#article_cartouche_chapeau {
top: 220px;
left:0;
padding:20px 24px 16px 60px;
max-width:560px;
height:auto;
max-height:100px;
}
}
@media (min-width:768px) and (max-width:979px) {
height:384px;
#container_socialtags {
top: 30px;
right: 45px;
}
#article_cartouche_titre{
top:30px; left:45px; min-width: 260px; max-width:460px;
h1 {
margin:24px 0;
font-size:26px;
line-height:36px;
}
h1::before{
content:'';
position:absolute;
width:80px;
height:12px;
top:0;
left:0;
border-left:6px solid $gris0;
border-bottom:6px solid $gris0;
}
h1::after{
content:'';
position:absolute;
width:200px;
height:12px;
bottom:4px;
right:0;
border-right:6px solid $gris0;
border-top:6px solid $gris0;
}
}
#article_cartouche_mots_cles{
left:45px;
top:280px;
width:460px;
ul li {
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
#article_cartouche_chapeau {
top: 140px;
left:0;
padding:20px 24px 16px 50px;
max-width:480px;
height:auto;
max-height:80px;
}
}


@media (max-width:767px) {
background-color:$gris12;
height:auto;
min-height:384px;
#container_socialtags {
top: 5%;
right: 5%;
padding:1.5%;
max-width:18%;
}
#article_cartouche_titre{
top:5%; left:5%; min-width: 30%; max-width:70%;
h1 {
margin:22px 0;
font-size:24px;
line-height:34px;
}
h1::before{
display:none;
content:'';
position:absolute;
width:60px;
height:10px;
top:0;
left:0;
border-left:5px solid $gris0;
border-bottom:5px solid $gris0;
}
h1::after{
display:none;
content:'';
position:absolute;
width:150px;
height:10px;
bottom:4px;
right:0;
border-right:5px solid $gris0;
border-top:5px solid $gris0;
}
}
#article_cartouche_mots_cles{
left:5%;
top:65%;
width:80%;
ul li {
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
#article_cartouche_chapeau {
top:30%;
left:0;
padding:4% 5% 4% 5%;
max-width:75%;
height:auto;
max-height:20%;
}
}
}

/* Styles du formulaire slogan, recherche, mots-clés */
#sommaire_recherche_educnat {
background-color:$gris12;
background-size: 100%;
display: block;
position: relative;
width:100%;
margin:30px 0 60px 0;
padding:0;
box-shadow: inset 0 0 1200px 1200px rgba(0, 0, 0, .1);
#sommaire_recherche_educnat_slogan_container{
display:block;
position: absolute;
height:auto;
#sommaire_recherche_educnat_slogan{
h2.slogan {font-family:'Archive', Arial, Helvetica, sans-serif;
color:#fff;
text-shadow: 0 0 1px $gris10;
font-weight:400;}
}
}
#sommaire_recherche_educnat_formulaire_recherche{
display:block;
position:absolute;
#formulaire_recherche {
border:none;
background-color:transparent;
box-shadow:none;
}
label {display:none;}
.input-append{background-color:transparent;}
input.search {
border-radius:0;
border:none;
}
}
#sommaire_recherche_educnat_mots_cles{
display:block;
position:absolute;
ul {display:inline-block; margin:0; padding:0; list-type:none;
li {display:inline; margin-right:8px; margin-bottom:30px; color:$gris0;
a {white-space: nowrap; line-height: 46px;}
}
}
}
}

@media (min-width: 1200px){
#sommaire_recherche_educnat {height:600px}
#sommaire_recherche_educnat_slogan_container{
top:80px; left:120px; width:460px; height:auto;
#sommaire_recherche_educnat_slogan{
margin:30px 0;
h2.slogan {font-size:32px;
line-height:42px;}
}
#sommaire_recherche_educnat_slogan::before {
content:'';
position:absolute;
width:100px;
height:14px;
top:0;
left:0;
border-left:7px solid $gris0;
border-bottom:7px solid $gris0;
}
#sommaire_recherche_educnat_slogan::after {
content:'';
position:absolute;
width:320px;
height:14px;
bottom:4px;
right:0;
border-right:7px solid $gris0;
border-top:7px solid $gris0;
}
}
#sommaire_recherche_educnat_formulaire_recherche{top:320px;
left:120px;
input.search {
padding:20px;
width:460px;
height:30px;
line-height:30px;
font-size:16px;
vetical-align:middle;
}
button.btn {
height:70px;
width:80px;
line-height:50px;
padding:10px;
font-size:16px;
vetical-align:middle;
border-radius:0;
border:none;
box-shadow:none;
border-left:1px solid $couleur1;
}
}
#sommaire_recherche_educnat_mots_cles{
top:440px; left:120px; width:700px;
display:block;
ul {margin:0;
padding:0;
list-type:none;
li{
margin-right:8px;
margin-bottom:30px;
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
}
}
@media (min-width: 980px) and (max-width: 1199px) {
#sommaire_recherche_educnat {height:490px}
#sommaire_recherche_educnat_slogan_container{
top:65px; left:100px; width:440px; height:auto;
#sommaire_recherche_educnat_slogan{
margin:30px 0;
h2.slogan {font-size:30px;
line-height:38px;}
}
#sommaire_recherche_educnat_slogan::before {
content:'';
position:absolute;
width:90px;
height:14px;
top:0;
left:0;
border-left:7px solid $gris0;
border-bottom:7px solid $gris0;
}
#sommaire_recherche_educnat_slogan::after {
content:'';
position:absolute;
width:280px;
height:14px;
bottom:4px;
right:0;
border-right:7px solid $gris0;
border-top:7px solid $gris0;
}
}
#sommaire_recherche_educnat_formulaire_recherche{top:240px;
left:100px;
input.search {
padding:20px;
width:400px;
height:30px;
line-height:30px;
font-size:16px;
vetical-align:middle;
}
button.btn {
height:70px;
width:80px;
line-height:50px;
padding:10px;
font-size:16px;
vetical-align:middle;
border-radius:0;
border:none;
box-shadow:none;
border-left:1px solid $couleur1;
}
}
#sommaire_recherche_educnat_mots_cles{
top:360px; left:100px; width:600px;
display:block;
ul { margin:0;
padding:0;
list-type:none;
li{
margin-right:8px;
margin-bottom:30px;
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
}
}
@media (min-width: 768px) and (max-width: 979px) {
#sommaire_recherche_educnat {height:384px}
#sommaire_recherche_educnat_slogan_container{
top:50px; left:80px; width:400px; height:auto;
#sommaire_recherche_educnat_slogan{
margin:26px 0;
h2.slogan {font-size:26px;
line-height:34px;}
}
#sommaire_recherche_educnat_slogan::before {
content:'';
position:absolute;
width:80px;
height:13px;
top:0;
left:0;
border-left:7px solid $gris0;
border-bottom:7px solid $gris0;
}
#sommaire_recherche_educnat_slogan::after {
content:'';
position:absolute;
width:240px;
height:13px;
bottom:4px;
right:0;
border-right:7px solid $gris0;
border-top:7px solid $gris0;
}
}
#sommaire_recherche_educnat_formulaire_recherche{top:194px;
left:80px;
input.search {
padding:12px;
width:370px;
height:20px;
line-height:20px;
font-size:16px;
vetical-align:middle;
}
button.btn {
height:44px;
width:50px;
line-height:20px;
padding:10px;
font-size:16px;
vetical-align:middle;
border-radius:0;
border:none;
box-shadow:none;
border-left:1px solid $couleur1;
}
}
#sommaire_recherche_educnat_mots_cles{
top:280px; left:80px; width:500px;
display:block;
ul {
margin:0;
padding:0;
list-type:none;
li{
margin-right:8px;
margin-bottom:30px;
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
}
}
@media (max-width: 767px) {
#sommaire_recherche_educnat {height:383px;}
#sommaire_recherche_educnat_slogan_container{
top:5%; left:5%; width:80%; height:auto;
#sommaire_recherche_educnat_slogan{
margin:0;
h2.slogan {font-size:20px;
line-height:28px;}
}
#sommaire_recherche_educnat_slogan::before {
display:none;
content:'';
position:absolute;
width:60px;
height:12px;
top:0;
left:0;
border-left:5px solid $gris0;
border-bottom:5px solid $gris0;
}
#sommaire_recherche_educnat_slogan::after {
display:none;
content:'';
position:absolute;
width:180px;
height:12px;
bottom:3px;
right:0;
border-right:5px solid $gris0;
border-top:5px solid $gris0;
}
}
#sommaire_recherche_educnat_formulaire_recherche{
top:35%;
left:5%;
input.search {
padding:12px;
width:80%;
height:18px;
line-height:18px;
font-size:14px;
vetical-align:middle;
}
button.btn {
height:42px;
width:46px;
line-height:18px;
padding:10px;
font-size:14px;
vetical-align:middle;
border-radius:0;
border:none;
box-shadow:none;
border-left:1px solid $couleur1;
}
}
#sommaire_recherche_educnat_mots_cles{
top:55%; left:5%; width:85%;
display:block;
ul {
display:inline-block;
margin:0;
padding:0;
list-type:none;
li{
display:inline;
margin-right:8px;
margin-bottom:30px;
a{
font-size:16px;
padding: 6px 14px;
color:$gris0;
border:2px solid $gris0;
border-radius : 28px;
background-color:rgba(0, 0, 0, 0.4);
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris10;
border:2px solid $gris0;
background-color: rgba(255, 255, 255, 0.8);
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
}
}

/* -- Styles des derniers articles en page de sommaire -- */
#sommaire_derniers_articles {
display:block;
margin:0 0 40px 0;
padding:0;
ul{
list-style:none;
display:block;
margin:0;
padding:0;
}
ul#liste-a-gauche{float:left;}
ul#liste-a-droite{float:right;}
li.item {
width : 100%;
padding:0;
box-sizing: border-box;
border:1px solid $gris2;
}
}
.presentation_article_logo {
display:block; position:relative;
padding:0; margin:0;
img {display:block; position:relative; padding:0; margin:0;}
}
.presentation_article_logo::after {
content:'';
position:absolute;
height:6px;
bottom:0;
left:0;
background-color: $couleur1;
}
.presentation_article_titre {
margin:30px 20px 20px 20px;
padding:0;
font-size:18px;
font-family:'Archive', Arial, Helvetica, sans-serif;
color:$gris10;
font-weight:800;
a {color:$gris10;}
}
.presentation_article_mots_cles_container{
display:block;
position:relative;
margin:40px 20px;
padding:0;
}
ul.presentation_article_mots_cles {
display:block;
margin:0;
padding:0;
list-type:none;
li{
display:inline-block;
margin-right:8px;
margin-bottom:30px;
a{
font-size:16px;
line-height:16px;
padding: 6px 14px;
color:$gris9;
border:2px solid $gris12;
border-radius : 28px;
white-space: nowrap;
transition: background-color 350ms ease-out 100ms, color 350ms ease-out 100ms, border-color 350ms ease-out 100ms;
}
a:hover{
text-decoration:none;
color:$gris0;
border:2px solid $couleur1;
background-color: $couleur1;
transition: background-color 350ms ease-in 100ms, color 350ms ease-in 100ms, border-color 350ms ease-in 100ms;
}
}
}
.presentation_article_introduction {
margin:50px 20px 40px 20px;
padding:0;
a{color:$gris9;}
}
a.presentation_article_bouton {display:block; position:relative; margin:20px; padding:16px; font-size:1em; text-align:center;}
a.presentation_article_bouton.un {color:$gris0; padding:20px 16px; background-color:$couleur1; transition: background-color 350ms ease-out 100ms;}
a.presentation_article_bouton.un:hover {background-color:darken($couleur1,$couleur1degrade3); transition: background-color 350ms ease-in 100ms; text-decoration:none;}
.presentation_article_bouton.deux{border:2px solid $couleur1; color:$couleur1; background-color:$gris0;
transition: background-color 350ms ease-out 100ms;}
a.presentation_article_bouton.deux:hover {background-color:$gris1; transition: background-color 350ms ease-in 100ms; text-decoration:none;
}

@media (min-width: 1200px){
#sommaire_derniers_articles {
width:1200px;
ul {
width:590px;
li.item {margin: 0 0 20px 0;}
}
}
.presentation_article_logo {
width:588px;
img {width:588px;}
}
.presentation_article_logo::after {width:588px;}
.presentation_article_mots_cles_container{width:568px;}
ul.presentation_article_mots_cles {max-width:548px;}
a.presentation_article_bouton {width:516px;}
.presentation_article_bouton.deux{width:512px;}
ul#liste-complete {display:none;}
}
@media (min-width: 980px) and (max-width: 1199px) {
#sommaire_derniers_articles {
width:980px;
ul {
width:480px;
li.item {margin: 0 0 20px 0;}
}
}
.presentation_article_logo {
width:478px;
img {width:478px;}
}
.presentation_article_logo::after {width:478px;}
.presentation_article_mots_cles_container {width:438px;}
ul.presentation_article_mots_cles {max-width:418px;}
a.presentation_article_bouton {width:406px;}
.presentation_article_bouton.deux {width:402px;}
ul#liste-complete {display:none;}
}
@media (min-width: 768px) and (max-width: 979px) {
#sommaire_derniers_articles {
width:768px;
ul {
width:374px;
li.item {margin: 0 0 20px 0;}
}
}
.presentation_article_logo {
width:372px;
img {width:372px;}
}
.presentation_article_logo::after {width:372px;}
.presentation_article_mots_cles_container {width:362px;}
ul.presentation_article_mots_cles {max-width:342px;}
a.presentation_article_bouton {width:300px;}
.presentation_article_bouton.deux {width:296px;}
ul#liste-complete {display:none;}
}
@media (max-width: 767px) {
#sommaire_derniers_articles {
width:100%;
ul#liste-complete {
margin-left:0; margin-right:0; padding-left:0; padding-right:0;
width:100%;
li.item {margin: 0 0 20px 0; padding-left:0; padding-right:0;}
}
}
.presentation_article_logo {
margin-left:0; margin-right:0; padding-left:0; padding-right:0;
width:100%;
a {
width:100%;
padding:0; margin:0;
img {width:100%; margin:0; padding:0;}
}
}
.presentation_article_logo::after {width:100%;}
.presentation_article_mots_cles_container {width:~\"calc(100% - 40px)\";}
a.presentation_article_bouton.un  {width:~\"calc(100% - 72px)\";}
a.presentation_article_bouton.deux {width:~\"calc(100% - 74px)\";}
ul#liste-a-gauche, ul#liste-a-droite {display:none;}
}

/* -- Titres -- */
#container_plan h1, .mot_blocs h2.h2, #content h1#page_mot_titre, #aside h2.h2, #extra h2.h2, #content h2.h2, h2.h2, h2.content_educnat, h2.menu-titre, #auteur_presentation h1 {
color:$couleur1;
font-family: 'Archive', Arial, Helvetica, sans-serif;
font-size: 24px;
line-height: 24px;
margin-left:90px;
}
#container_plan h1::before, div.liste.mots h2.h2::before, #aside h2.h2::before, #extra h2.h2::before, h2.menu-titre::before, #auteur_presentation h1::before, #content h2.h2::before, h2.content_educnat::before, h1#page_mot_titre::before {
content:'';
position:absolute;
width:70px;
height:6px;
top:9px;
left:0;
background-color:$couleur1;
}
.container_h2 {display:block; position:relative; margin:0; padding:0;}

/* -- Carousel container -- */
#sommaire_carousel_container {
display:block;
position:relative;
margin:50px auto 60px auto;
}
@media (min-width: 1200px){
#sommaire_carousel_container {width:1200px;}
}
@media (min-width: 980px) and (max-width: 1199px) {
#sommaire_carousel_container {width:980px;}
#header .carousel strong a span.titre, #content .carousel strong a span.titre {
font-size:22px;
line-height:22px;
}
}
@media (min-width: 768px) and (max-width: 979px) {
#sommaire_carousel_container {width:768px;}
#header .carousel strong a span.titre, #content .carousel strong a span.titre {
font-size:20px;
line-height:20px;
}
}
@media (max-width: 767px) {
#sommaire_carousel_container {width:100%;}
#header .carousel strong a span.titre, #content .carousel strong a span.titre {
font-size:22px;
line-height:22px;
}
}
.container_titre_carousel {
display:block;
position: absolute;
top:0;
left:0;
margin:0;
max-width:440px;
padding:46px 0 40px 40px;
}
@media (min-width: 1200px) {
#header .carousel strong a span.titre::before, #content .carousel strong a span.titre::before {
content:'';
position:absolute;
width:90px;
height:12px;
top:18px;
left:40px;
border-left:6px solid $gris0;
border-bottom:6px solid $gris0;
}
#header .carousel strong a span.titre::after, #content .carousel strong a span.titre::after {
content:'';
position:absolute;
width:130px;
height:12px;
bottom:12px;
right:0;
border-right:6px solid $gris0;
border-top:6px solid $gris0;
}
}
@media (min-width: 980px) and (max-width: 1199px) {
#header .carousel strong a span.titre, #content .carousel strong a span.titre {
display:block;
position:absolute;
top:10px;
left:10px;
max-width:400px;
}
}
@media (min-width: 768px) and (max-width: 979px) {
#header .carousel strong a span.titre, #content .carousel strong a span.titre {
display:block;
position:absolute;
top:6px;
left:6px;
max-width:312px;
}
}

/* ------------------------------------------ */
/* -------- Adaptations carousel -------- */
/* ------------------------------------------ */

#header .carousel, #content .carousel {background-color:$couleur1; border:1px solid $couleur1; box-shadow: -1px 2px 6px 3px darken($couleur1, $couleur1degrade4) inset;}
#header .carousel strong a, #content .carousel strong a {margin:0; padding:0; font-weight:400;}
#header .carousel strong a span.titre, #content .carousel strong a span.titre {font-weight:400; color:$gris0; font-family:'Archive', Arial, Helvetica, sans-serif; margin:0; padding:0; transition: background-color 350ms ease-out 100ms;}
#header .carousel strong a:hover, #content .carousel strong a:hover{background-color:transparent;}
#header .carousel strong a span.titre:hover, #content .carousel strong a span.titre:hover {
background-color:darken($couleur1, $couleur1degrade3);
color:$gris0;
transition: background-color 350ms ease-in 100ms;
}
#header .carousel .introduction, #content .carousel .introduction {
position:absolute;
background-color: rgba(0, 0, 0, 0.4);
color:$gris0;
bottom: 40px;
left:0;
padding:16px 6px 10px 26px;
margin:0;
max-width:400px;
max-height:70px;
overflow:hidden;
}
@media (min-width: 768px) and (max-width:980px) {
#header .carousel .introduction, #content .carousel .introduction { max-width:320px;
}
}
@media (min-width: 1200px) {
	#header .carousel, #content .carousel, #header .carousel .carousel-inner .item article, #content .carousel .carousel-inner .item article  {height:279px;}
	.de_980_a_1200,.de_768_a_980,.moins_de_768 {display:none;}
	.carousel .carousel-stop {left:12%;}
	.carousel ol.carousel-indicators{left:16%;}
}

@media (min-width: 980px) and (max-width:1199px) {
	#header .carousel, #header .carousel .carousel-inner .item article, #content .carousel, #content .carousel .carousel-inner .item article  {height:228px;}
	#header .carousel .carousel-inner .item article span.img, #content .carousel .carousel-inner .item article span.img {width:555px; height:228px;}
	.plus_de_1200,.de_768_a_980,.moins_de_768 {display:none;}
	.carousel .carousel-stop {left:12%;}
	.carousel ol.carousel-indicators{left:16%;}
}

@media (min-width: 768px) and (max-width:980px) {
	#header .carousel, #header .carousel .carousel-inner .item article, #content .carousel, #content .carousel .carousel-inner .item article  {height:178px;}
	#header .carousel .carousel-inner .item article span.img, #content .carousel .carousel-inner .item article span.img {width:555px; height:178px;}
	.plus_de_1200,.de_980_a_1200,.moins_de_768 {display:none;}
	.carousel .carousel-stop {left:12%;}
	.carousel ol.carousel-indicators{left:16%;}
}
@media (max-width:767px) {
	#header .carousel .carousel-inner .item article  strong.moins_de_768, #content .carousel .carousel-inner .item article  strong.moins_de_768 {height:350px; text-align:center;}
	#header .carousel .carousel-inner .item article  strong.moins_de_768 .logo_moins_de_768, #content .carousel .carousel-inner .item article  strong.moins_de_768 .logo_moins_de_768 {
		display:block;
		position:relative;
		float:left;
		width:100%;
	max-height:150px;
	}
	#header .carousel .carousel-inner .item article strong.moins_de_768 .logo_moins_de_768 img, #content .carousel .carousel-inner .item article strong.moins_de_768 .logo_moins_de_768 img{
		text-align:center;
		max-height:150px;
	}
	#header .carousel .carousel-inner .item article  strong.moins_de_768, #content .carousel .carousel-inner .item article  strong.moins_de_768  .titre_moins_de_768 {
		display:block;
		position:relative;
		width:100%;
		float:left;
		text-align:center; 
		min-height:50px;
	}
	.plus_de_1200,.de_980_a_1200,.de_768_a_980 {display:none;}
	.carousel .carousel-stop {left:42%;}
	.carousel ol.carousel-indicators{left:50%;}
}

/* -- CSS de l'entête -- */
div#header_educnat header.accueil h1{
margin:0 auto;
padding:0;
img {display:block; position:relative;margin:28px 0; padding:0; z-index:999;}
}

/* -- CSS du popin accessibilité -- */
@media (min-width:768px) {
.recherche_popin_accessibilite_petits_ecrans {display:none;}
}
@media (max-width:767px) {
#bloc_acces_recherche {
display:none;
}
.recherche_popin_accessibilite_petits_ecrans {
font-family: 'icomoon';
}
}
ul#liste_dyslexie, ul#liste_interlignage, ul#liste_justification {margin-left:10px;}
#popin_accessibilite, #popin_accessibilite legend, button#cboxClose {font-family:'Roboto';}
.fieldset_dys {margin:10px;}
.div_dys {display:flex;flex-direction:row; flex-wrap:wrap;}
#legend_dys,#legend_justification,#legend_interligne {font-size:1.5em;}

#popin_accessibilite h2#titre_accessibilite {
font-family: 'Archive', Arial, Helvetica, sans-serif;
font-size: 19px;
line-height: 24px;
padding-top:20px;
color:darken($couleur1, $couleur1degrade5);
a{color:darken($couleur1, $couleur1degrade5);}
body a:hover{color:darken($couleur1, $couleur1degrade5); background-color:transparent; text-decoration:underline;}
}
#popin_accessibilite h2#titre_accessibilite:before { 
content:'';
position:absolute;
width:43px;
height:3px;
background-color:darken($couleur1, $couleur1degrade5);
left:10px;
top:20px;}

/* -- CSS du menu horizontal -- */

.navbar-inner {border:none; box-shadow:none; padding-left:0; padding-right:0;}
div#nav {margin-left:0; padding-left:0;}
div#nav ul.menu-liste>li>a {box-sizing: content-box; padding:10px 0; margin:0 40px 0 0; border-top:2px solid $gris2; text-shadow:none; position:sticky;}
div#nav ul.menu-liste>li>a:hover, div#nav ul.menu-liste>li.on>a, div#nav ul.menu-liste>li.home>a.on {border-top:2px solid $gris10; font-weight:normal; background-color:transparent; box-shadow:none; text-shadow:none;}
div#nav ul.menu-liste>li.on a {font-weight:normal;}

/* -- CSS de l'entête en cas de scroll -- */

#suite-navbar-reduction {display:none; position:relative; width:100%; height:140px;}
#header_educnat {display:block;}
div#nav #navbar-reduction.actif ul.menu-liste>li>a {margin:0 20px 0 0;}
#navbar-reduction.actif {
position:fixed;
z-index:1000;
top:0;
width:100%;
padding-top:40px;
padding-bottom:20px;
border-bottom:1px solid $gris5;
box-shadow: 0px 5px 5px 0 $gris5;
background-color:$gris0;}

#container-menu-reduit {
display:flex;
flex-direction: row;
margin-left:auto;
margin-right:auto;
}

#icone_mariane_reduit {
display:none;
width:140px;
background-image:url('".$GLOBALS['meta']['adresse_site'].str_replace ('..','',_DIR_PLUGIN_SPIPR_EDUC)."themes/educnat/images/mariane.png');
background-position: 21px left;
background-repeat:no-repeat;
}

#bloc_acces_recherche {
font-family: 'icomoon';
text-align:right;
color:$gris9;
width:140px;
padding-top:10px;
a {width: 24px; height:24px; padding:8px; color:$gris10; line-height:24px; font-size: 24px; text-align:center; border-radius:50%; border:1px solid $gris2;
-webkit-transform: perspective(1px) translateZ(0);
transform: perspective(1px) translateZ(0);
-webkit-transition-duration: 0.3s;
transition-duration: 0.3s;
-webkit-transition-property: color, background-color;
transition-property: color, background-color;}
a:first-child {margin-right:24px;}
a:hover {color:#fff; background-color:darken($couleur1, $couleur1degrade5); border-color:darken($couleur1, $couleur1degrade5); text-decoration:none;}
}

/* -- CSS du menu déplié -- */

@media (min-width: 768px) {
#nav .menu-container .nav .dropdown-menu {
width:600px;
padding:0;
border-radius: 0;
box-shadow: 0 0 10px $gris7;
border : 1px solid $gris5;

.dropdown-menu-container {
display:flex;
flex-direction:row;

.dropdown-menu-premier {
width:224px;
border-right:4px solid $couleur1;
padding:50px 35px 35px 35px;
h2 {
font-family: 'Archive', Arial, Helvetica, sans-serif;
font-size: 19px;
line-height: 24px;
color:darken($couleur1, $couleur1degrade5);
a{color:darken($couleur1, $couleur1degrade5);}
a:hover{color:darken($couleur1, $couleur1degrade5); background-color:transparent; text-decoration:underline;}
}
h2:before { 
content:'';
position:absolute;
width:43px;
height:3px;
background-color:darken($couleur1, $couleur1degrade5);
left:35px;
top:40px;}
.h2-rubrique-descriptif {
font-family: 'Roboto', Arial, Helvetica, sans-serif;
font-size: 13px;
line-height: 23px;
font-weight:normal;
margin-top:20px;
}
}
.dropdown-menu-second-large {
max-height:~\"calc(100vh - 280px)\";
}
.dropdown-menu-second-court {
max-height:~\"calc(100vh - 100px)\";
}
.dropdown-menu-second {
width:300px;
overflow-y: auto;
ul.menu-dropdown-menu-second {
margin:35px;
list-style:none;
list-style-position: outside;
&>li {padding-bottom:14px;}
li a {
text-decoration:underline;
color:$gris10;
}
li a:hover {
color:$couleur1;
background-color:transparent;
}
li.on a, li.active a {font-weight:normal;}
ul {
list-style-type:disc;
margin-left:10px;
list-style-position: inside;
}
}
}
}
}
}

/* --CSS du menu déplié sur petits écrans-- */
@media (max-width: 767px) {
div#nav ul.menu-liste>li>a {max-width:none; font-weight:normal;}
div#nav ul.menu-liste>li>a:hover, div#nav ul.menu-liste>li>a:on {font-weight:bolder;}

#nav .menu-container .nav .dropdown-menu {
padding:0;
border-radius: 0;
box-shadow: 0 0 10px $gris7;
border : 1px solid $gris5;
margin-bottom:20px;

.dropdown-menu-container {
display:flex;
flex-direction:column;

.dropdown-menu-premier {
border-bottom:4px solid $couleur1;
padding:50px 35px 35px 35px;
h2 {
text-indent:0;
padding-left:0;
margin-left:0;
font-family: 'Archive', Arial, Helvetica, sans-serif;
font-size: 19px;
line-height: 24px;
color:darken($couleur1, $couleur1degrade5);
a{color:darken($couleur1, $couleur1degrade5);
text-indent:0;
padding-left:0;
margin-left:0;
font-weight:normal;}
a:hover{color:darken($couleur1, $couleur1degrade5); background-color:transparent; text-decoration:underline;}
}
h2:before { 
content:'';
position:absolute;
width:43px;
height:3px;
background-color:darken($couleur1, $couleur1degrade5);
left:50px;
top:83px;}
.h2-rubrique-descriptif {
font-family: 'Roboto', Arial, Helvetica, sans-serif;
font-size: 13px;
line-height: 23px;
font-weight:normal;
margin-top:20px;
}
}
.dropdown-menu-second-large {
max-height:~\"calc(100vh - 300px)\";
}
.dropdown-menu-second-court {
max-height:~\"calc(100vh - 300px)\";
}
.dropdown-menu-second {
overflow-y: auto;
ul.menu-dropdown-menu-second {
margin:35px;
margin-bottom:30px;
list-style:none;
list-style-position: outside;
&>li {padding-bottom:14px;}
li a {
text-decoration:underline;
color:$gris10;
text-indent:0;
padding-left:0;
margin-left:0;
font-weight:normal;
}
li a:hover {
color:$couleur1;
background-color:transparent;
}
li.on a, li.active a {font-weight:normal;}
ul {
list-style-type:disc;
margin-left:10px;
list-style-position: inside;
}
}
}
}
}
}

/* -- Autres -- */

body {font-family: 'Roboto',Arial,Helvetica,sans-serif;}
a:hover {text-decoration:underline; background-color:transparent;}
.container {width:100%; margin:0; padding:0;}
#liens_partenaires, #header_educnat, .content_educnat, ul#pied, ul#footer-plain {margin-left:auto; margin-right:auto;}
.ecriture_roboto {font-family:'Roboto';}

/* -- CSS du pied de page -- */

#footer {width:100%; padding:30px 0 0 0; margin:40px 0 0 0; border-top:1px solid $gris2;}
#pied {position : relative; clear:both; display:block;}
ul#pied  {padding :0; list-style:none; margin-top:0; margin-bottom:0}
ul#pied li {padding:0; margin:0; text-indent:0; list-style:none; float:left;  text-align:left;}
ul#pied .pied_label {text-transform:uppercase; float:left; font-family: 'Archive',Arial,Helvetica,sans-serif; margin-bottom:16px; font-size:1.2em;}
#pied li.pied_niv2 {float:left; text-align:left; margin:0; padding:7px 0; line-height:140%; width:100%}
#pied li.pied_niv2, #pied li.pied_niv2 a {text-decoration:none; color:$gris10; background-color:transparent;}
#pied li.pied_niv2 a:hover {text-decoration:underline; background-color:transparent;}
ul#pied ul.pied_liste {margin-left:0;}
ul#footer-plain {
display:block; border-top:1px solid $gris2; margin-top:40px; margin-bottom:0;
padding:20px 0;
li {padding:0; margin:0; float:left; list-style:none;
a {font-size:92%; text-decoration:none; color:$gris10;padding:6px;}
}
li:first-child a{padding-left:0;}
li.footer_separation::after {
    content: '|'; color:$gris10;
}
li.footer_copyright{
width:100%; float:left; color:$gris78;
}
}

/* -- Spécificités en fonction des largeurs d'écran -- */
@media (min-width: 1200px) {
#content {width:1200px;}
#aside {width:580px;}
#extra {width:580px;}
#liens_partenaires, #header_educnat, .content_educnat, ul#pied, ul#footer-plain {width:1200px;}
ul#pied li.pied_cat {padding: 2% 3%; width:17%; background:none;  float:left; text-align:left; margin:0;}
ul#pied li.pied_cat:first-child {padding-left:0;}
ul#pied li.pied_cat:last-child {padding-right:0;}
.span12, div#header_educnat header.accueil h1#logo_site_spip, #container-menu-reduit {width:1200px;}
.div_dys {width:560px;}
.fieldset_dys {width:260px;}
#navbar-reduction ul.span12 {width:1060px;}
#navbar-reduction.actif ul.span12 {width:920px;}
}

@media (min-width: 980px) and (max-width: 1199px) {
#content {width:980px;}
#aside {width:470px;}
#extra {width:470px;}
#liens_partenaires, #header_educnat, .content_educnat, ul#pied, ul#footer-plain {width:980px;}
ul#pied li.pied_cat {padding:2%; width:20%; background:none;  float:left; text-align:left; margin:0;}
ul#pied li.pied_cat:first-child {padding-left:2%;}
ul#pied li.pied_cat:last-child {padding-right:2%;}
.span12, div#header_educnat header.accueil h1#logo_site_spip, #container-menu-reduit {width:980px;}
.div_dys {width:500px;}
.fieldset_dys {width:230px;}
#navbar-reduction ul.span12 {width:840px;}
#navbar-reduction.actif ul.span12 {width:700px;}
}

@media (min-width: 768px) and (max-width: 979px) {
#content {width:768px;}
#aside {width:369px;}
#extra {width:369px;}
#liens_partenaires, #header_educnat, .content_educnat, ul#pied, ul#footer-plain {width:768px;}
ul#pied li.pied_cat {padding:2%; width:29%; background:none;  float:left; text-align:left; margin:0;}
ul#pied li.pied_cat:first-child {padding-left:2%;}
ul#pied li.pied_cat:last-child {padding-right:2%;}
.span12, div#header_educnat header.accueil h1#logo_site_spip, #container-menu-reduit {width:768px;}
.div_dys {width:260px;}
.fieldset_dys {width:240px;}
#navbar-reduction ul.span12 {width:628px;}
#navbar-reduction.actif ul.span12 {width:488px;}
}

@media (min-width: 451px) and (max-width: 767px) {
#content {width:100%;}
#aside {width:~\"calc(50%-10px)\";}
#extra {width:~\"calc(50%-10px)\";}
#liens_partenaires, #header_educnat, .content_educnat, ul#pied, ul#footer-plain {width:100%;}
ul#pied li.pied_cat {padding:2%; width:45%; background:none;  float:left; text-align:left; margin:20px 0 0 0;}
ul#pied li.pied_cat:first-child {padding-left:2%;}
ul#pied li.pied_cat:last-child {padding-right:2%;}
.span12, div#header_educnat header.accueil h1#logo_site_spip, #container-menu-reduit {width:100%;}
.div_dys {width:260px;}
.fieldset_dys {width:240px;}
}

@media (max-width: 450px) {
#content, #aside, #extra {width:100%;}
#liens_partenaires, #header_educnat, .content_educnat, ul#pied, ul#footer-plain {width:100%;}
ul#pied li.pied_cat {padding:2% 4%; width:86%; background:none;  float:left; text-align:left; margin:20px 0 0 0;}
ul#pied li.pied_cat:first-child {padding-left:4%;}
ul#pied li.pied_cat:last-child {padding-right:4%;}
.span12, div#header_educnat header.accueil h1#logo_site_spip, #container-menu-reduit {width:100%;}
.div_dys {width:50%;}
.fieldset_dys {width:~\"calc(100%-20px)\";}
}

/* -- De l'air !!! -- */

#container_plan, #container_mot_spipr_educ, .mot_blocs, #content .mot_blocs, #aside .mot_blocs, #extra .mot_blocs  {display:block; position:relative; margin-top:50px;}

#site_presentation, #site_nuage, #site_mini_calendrier, #site_menu_mots_cles, #site_liens_partenaires, #site_evenements, #site_dyslexie, #site_bloc_perso_1, #site_bloc_perso_2, #site_bloc_perso_3, #site_bloc_logos, #site_autres_sites, #site_articles_syndiques, #breve_nuage, #breve_mini_calendrier, #breve_meme_rubrique, #breve_liens_partenaires, #breve_forum, #breve_evenements, #breve_dyslexie, #breve_contenu, #breve_bloc_logos, #breve_bloc_perso_1, #breve_bloc_perso_2, #breve_bloc_perso_3, #sommaire_twitter, #sommaire_nuage, #sommaire_mini_calendrier, #sommaire_liens_partenaires, #sommaire_inscription, #sommaire_forums, #sommaire_evenements, #sommaire_editorial, #sommaire_dyslexie, #sommaire_derniers_sites, #sommaire_derniers_articles, #sommaire_dernieres_breves, #sommaire_compteur, #sommaire_acces_restreint, #sommaire_article_hero, #sommaire_articles_syndiques, #sommaire_bloc_logos, #sommaire_bloc_perso_1, #sommaire_bloc_perso_2, #sommaire_bloc_perso_3, .espacer_secteurs, #autre_bloc_logos, #autre_bloc_perso_1, #autre_bloc_perso_2, #autre_bloc_perso_3, #autre_dyslexie, #autre_evenements, #autre_inscription, #autre_liens_partenaires, #autre_mini_calendrier, #autre_nuage, #autre_recherche, #auteur_presentation, #auteur_nuage, #auteur_mini_calendrier, #auteur_liens_partenaires, #auteur_evenements, #auteur_ecrire_auteur, #auteur_dyslexie, #auteur_bloc_perso_1, #auteur_bloc_perso_2, #auteur_bloc_perso_3, #auteur_bloc_logos, #auteur_articles, #auteur_autres_auteurs, .article_cartouche_meta, #rubrique_texte, #rubrique_sites, #rubrique_proposer_site, #rubrique_portfolio, #rubrique_plan, #rubrique_nuage, #rubrique_mots_cles, #rubrique_mini_calendrier, #rubrique_bloc_perso_1, #rubrique_bloc_perso_2, #rubrique_bloc_perso_3, #rubrique_bloc_logos, #rubrique_articles_syndiques, #rubrique_articles, #article_contenu, #rubrique_breves, #rubrique_dyslexie, #article_bloc_perso_1, #article_bloc_perso_2, #article_bloc_perso_3, #rubrique_evenements, #article_evenements, #article_evenements_article, #article_forum, #spipr_educ_liens_partenaires, #article_meme_rubrique, #article_mini_calendrier, #article_mots_cles, #article_nuage, #article_petition, #article_portfolio, #article_recherche {margin-bottom:70px;}

/* -- Bouton de retour en haut de page -- */
#backtotop {
font-family:  'icomoon' !important;
bottom:16px;
right:16px;
height:20px;
width:24px;
text-align:center;
line-height:20px;
vertical-align:middle;
font-size:20px;
padding:4px;
border-radius: 4px;
border: 2px solid $couleur1;
color:$couleur1;
background-color:$gris0;
transition: background-color 350ms ease-out 100ms;
}
#backtotop:hover {
background-color:$gris4;
transition: background-color 350ms ease-in 100ms;
}

",
					'parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
				),
				"type='graphisme' AND nom='graphisme_css' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
			);
			

			// Ensemble des entrées dont les 10 paramètres sont vidés
			$a_vider = array(
				// Les blocs perso
				'autre_bloc_perso_1','autre_bloc_perso_2','autre_bloc_perso_3',
				'auteur_bloc_perso_1','auteur_bloc_perso_2','auteur_bloc_perso_3',
				'site_bloc_perso_1','site_bloc_perso_2','site_bloc_perso_3',
				'breve_bloc_perso_1','breve_bloc_perso_2','breve_bloc_perso_3',
				'rubrique_bloc_perso_1','rubrique_bloc_perso_2','rubrique_bloc_perso_3',
				'article_bloc_perso_1','article_bloc_perso_2','article_bloc_perso_3',
				'sommaire_bloc_perso_1','sommaire_bloc_perso_2','sommaire_bloc_perso_3',
				
			);	
			foreach ($a_vider as $nom_bloc) {
				sql_updateq(
					'spip_spipr_educ',
					array(
						'parametre1' => '','parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
					),
					"type='graphisme' AND nom='$nom_bloc' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
				);
			}
			
			// Up du nom et de la déclinaison du thème $theme,$theme_de_couleur
			sql_updateq(
				'spip_spipr_educ',
				array(
					'nom' => $theme,'parametre1' => $theme_de_couleur,'parametre2' => '','parametre3' => '','parametre4' => '','parametre5' => '','parametre6' => '','parametre7' => '','parametre8' => '','parametre9' => '','parametre10' => '',
				),
				"type='theme' AND nom_sauvegarde='en_cours_d_utilisation_SPIPr'"
			);
			$retour = 'ok';
		break;
	}
	return $retour;
}